window.mainAccessPassword = '101658216322';

window.beepAudio = new Audio('../audio/beep.wav');
window.finishAlerAudio = new Audio('../audio/finishAlert.wav');
window.keyPressAudio = new Audio('../audio/keyPress.wav');

window.accessGrantedAudio = new Audio('../audio/access_granted.wav');
window.accessDeniedAudio = new Audio('../audio/access_denied.wav');

window.accessSemiAlertAudio = new Audio('../audio/semiAlert.mp3');
window.accessSemiAlertAudio.volume = 10/100;

window.accessAlertAudio = new Audio('../audio/alert.mp3');
window.accessAlertAudio.volume = 10/100;

window.soundActive = false;

//window.beepAudio.addEventListener('ended', function(){
//    window.soundActive = false;
//});

window.finishAlerAudio.addEventListener('ended', function(){
    window.soundActive = false;
});

function playBeepSound()
{
    //if (!window.soundActive) {
    //    window.soundActive = true;

        window.beepAudio.play();
    //}   
}

function playFinishAlertSound()
{
    if (!window.soundActive) {
        window.soundActive = true;

        window.finishAlerAudio.play();
    }   
}

function playKeyPressAudio()
{
    window.keyPressAudio.play();
}

$(document).ready(function(){
    
    $(document).on('contextmenu', function(e){
        e.preventDefault();
    })

    $('[data-beep-audio-effect]').on('click', function(){
        playBeepSound();
    })

    var finalScripts = [
        ':.main: *************** RSVP Agent started ***************',
        ':...locate_configFile: Specified configuration file: /u/user10/rsvpd1.conf',
        ':.main: Using log level 511',
        ':..settcpimage: Get TCP images rc - EDC8112I Operation not supported on socket.',
        ':..settcpimage: Associate with TCP/IP image name = TCPCS',
        ':..reg_process: registering process with the system',
        ':..reg_process: attempt OS/390 registration',
        ':..reg_process: return from registration rc=0',
        ':...read_physical_netif: Home list entries returned = 7',
        ':...read_physical_netif: index #0, interface VLINK1 has address 129.1.1.1, ifidx 0',
        ':...read_physical_netif: index #1, interface TR1 has address 9.37.65.139, ifidx 1',
        ':...read_physical_netif: index #2, interface LINK11 has address 9.67.100.1, ifidx 2',
        ':...read_physical_netif: index #3, interface LINK12 has address 9.67.101.1, ifidx 3',
        ':...read_physical_netif: index #4, interface CTCD0 has address 9.67.116.98, ifidx 4',
        ':...read_physical_netif: index #5, interface CTCD2 has address 9.67.117.98, ifidx 5',
        ':...read_physical_netif: index #6, interface LOOPBACK has address 127.0.0.1, ifidx 0',
        ':....mailslot_create: creating mailslot for timer',
        ':...mailbox_register: mailbox allocated for timer',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':.....mailslot_create: setsockopt(MCAST_ADD) failed - EDC8116I Address not available.',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 129.1.1.1, entity for rsvp allocated and initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':.....mailslot_create: setsockopt(MCAST_ADD) failed - EDC8116I Address not available.',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 9.37.65.139, entity for rsvp allocated and ',
        'initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':.....mailslot_create: setsockopt(MCAST_ADD) failed - EDC8116I Address not available.',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 9.67.100.1, entity for rsvp allocated and initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':.....mailslot_create: setsockopt(MCAST_ADD) failed - EDC8116I Address not available.',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 9.67.101.1, entity for rsvp allocated and initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 9.67.116.98, entity for rsvp allocated and ',
        'initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 9.67.117.98, entity for rsvp allocated and ',
        'initialized',
        ':.....mailslot_create: creating mailslot for RSVP',
        ':....mailbox_register: mailbox allocated for rsvp',
        ':.....mailslot_create: creating mailslot for RSVP via UDP',
        ':....mailbox_register: mailbox allocated for rsvp-udp',
        ':..entity_initialize: interface 127.0.0.1, entity for rsvp allocated and initialized',
        ':......mailslot_create: creating socket for querying route',
        ':.....mailbox_register: no mailbox necessary for forward',
        'tional socket',
        'tional socket connection',
        ':.....mailbox_register: mailbox allocated for route',
        ':.....mailslot_create: creating socket for traffic control module',
        ':....mailbox_register: no mailbox necessary for traffic-control',
        ':....mailslot_create: creating mailslot for RSVP client API',
        ':...mailbox_register: mailbox allocated for rsvp-api',
        ':...mailslot_create: creating mailslot for terminate',
        ':..mailbox_register: mailbox allocated for terminate',
        ':...mailslot_create: creating mailslot for dump',
        ':..mailbox_register: mailbox allocated for dump',
        ':...mailslot_create: creating mailslot for (broken) pipe',
        ':..mailbox_register: mailbox allocated for pipe',
        ':.main: rsvpd initialization complete',
        ':......rsvp_api_open: accepted a new connection for rapi',
        ':.......mailbox_register: mailbox allocated for mailbox',
        ':......rsvp_event_mapSession: Session=9.67.116.99:1047:6 does not exist',
        '.....api_reader: api request SESSION',
        ':......rsvp_event_establishSession: local node will send',
        ':........router_forward_getOI: Ioctl to get route entry successful',
        ':........router_forward_getOI:         source address:   9.67.116.98',
        ':........router_forward_getOI:         out inf:   9.67.116.98',
        ':........router_forward_getOI:         gateway:   0.0.0.0',
        ':........router_forward_getOI:         route handle:   7f5251c8',
        ':.......event_establishSessionSend: found outgoing if=9.67.116.98 through ',
        'forward engine',
        ':......rsvp_event_mapSession: Session=9.67.116.99:1047:6 exists',
        '.....api_reader: api request SENDER',
        ':.......init_policyAPI: papi_debug:  Entering',
        ':.......init_policyAPI: papi_debug:  papiLogFunc = 98681F0 papiUserValue = 0',
        ':.......init_policyAPI: papi_debug:  Exiting',
        ':.......init_policyAPI: APIInitialize:  Entering',
        ':.......init_policyAPI: open_socket:  Entering',
        ':.......init_policyAPI: open_socket:  Exiting',
        ':.......init_policyAPI: APIInitialize:  ApiHandle = 98BDFB0,  connfd = 22',
        ':.......init_policyAPI: APIInitialize:  Exiting',
        ':.......init_policyAPI: RegisterWithPolicyAPI:  Entering',
        ':.......init_policyAPI: RegisterWithPolicyAPI:  Writing to socket = 22',
        ':.......init_policyAPI: ReadBuffer:  Entering',
        ':.......init_policyAPI: ReadBuffer:  Exiting',
        ':.......init_policyAPI: RegisterWithPolicyAPI:  Exiting',
        ':.......init_policyAPI: Policy API initialized',
        ':......rpapi_getPolicyData: RSVPFindActionName:  Entering',
        ':......rpapi_getPolicyData: ReadBuffer:  Entering',
        ':......rpapi_getPolicyData: ReadBuffer:  Exiting',
        ':......rpapi_getPolicyData: RSVPFindActionName:  Result = 0',
        ':......rpapi_getPolicyData: RSVPFindActionName:  Exiting',
        
        ':......rpapi_getPolicyData: found action name CLCat2 for ',
        'flow[sess=9.67.116.99:1047:6,source=9.67.116.98:8000]',
        ':......rpapi_getPolicyData: RSVPFindServiceDetailsOnActName:  Entering',
        ':......rpapi_getPolicyData: ReadBuffer:  Entering',
        ':......rpapi_getPolicyData: ReadBuffer:  Exiting',
        ':......rpapi_getPolicyData: RSVPFindServiceDetailsOnActName:  Result = 0',
        ':......rpapi_getPolicyData: RSVPFindServiceDetailsOnActName:  Exiting',
        ':.....api_reader: appl chose service type 1',
        ':......rpapi_getSpecData: RSVPGetTSpec:  Entering',
        '---------------------------------Complete injecting task-------------------- ',
        'Wait for result...'
    ];

    for(var i = 0; i < finalScripts.length; i++) {
        $('[data-command-step-lib-final-hack]').append('<div class="step">Rewrite module attack to ' + generateId() + ' zone ' + finalScripts[i] + '</div>');
    }

    $('#enable-manual-mode').on('click', function(){
        $('.manual-mode-form').hide();
        $('.password-login-container').removeClass('hidden');
        $('#login-password').val('');
        $('#login-password').focus();
    })

    $('#confirm-password').on('click', function(e) {
        e.preventDefault();

        if ($('#login-password').val().trim() !== window.mainAccessPassword) {
            $('#login-password').addClass('error');
            $('#login-password').val('');
            window.accessDeniedAudio.play();
            
        } else {
            $('#login-password').removeClass('error');

            window.accessGrantedAudio.play();

            setTimeout(function () {
                $('.auto-mode-wrapper-container').fadeOut(400, function(){
                    $('.auto-mode-wrapper-container').addClass('hidden');
                    $('.service-item.auto-mode').removeClass('ok').addClass('error');
                });
                runRemoteHackerEmulator();
            }, 1000);
        }
    })

    window.vaultRoomServerSessionId = 0;
    window.typpingInterval = null;

    setInterval(function(){
        if(window.sessionStatus.receivedStatus.vault_room_is_hacked == 1) {
            $('.vault-room-hack-notification-container').removeClass('hidden');

            setTimeout(function(){
                $('.dali-bg-container').removeClass('hidden');
            }, 1000)
        } else {
            $('.vault-room-hack-notification-container').addClass('hidden');
            $('.dali-bg-container').addClass('hidden');
        }

        if (window.vaultRoomServerSessionId != window.sessionStatus.receivedStatus.session_id) {
            resetLoginLanding();
            window.vaultRoomServerSessionId = window.sessionStatus.receivedStatus.session_id;
        }

    }, 2000);

    function resetLoginLanding() {
        
        clearInterval(window.typpingInterval);
        $('.auto-mode-wrapper-container').removeClass('hidden');
        $('.auto-mode-wrapper-container').show();
        $('.manual-mode-form').show();
        $('.password-login-container').addClass('hidden');
        $('#login-password').val('');
        $('#login-password').removeClass('error');
        $('.service-item.auto-mode').addClass('ok').removeClass('error');
        $('.vault-room-hack-notification-container').addClass('hidden');
        $('.dali-bg-container').addClass('hidden');
    }
    

    function runRemoteHackerEmulator () {

        $('#commands-dictionary').find('.item').attr('data-text-pending', '');
        $('#commands-dictionary').find('.step').attr('data-step-pending', '');
        $('#commands-dictionary').find('.item').attr('data-command-writted', '');

        $('.input-container').find('p.input-line').remove();
        $('.output-container').find('p.output-line').remove();

        $('.input-container').append('<p class="input-line content-line"></p>');

        window.accessSemiAlertAudio.play();


        window.isNewCommand = true;
        window.isSshConnection = false;

        window.tippyngTimeInterval = 50;
        window.typpingInterval = setInterval(function(){

            if ($('#commands-dictionary').find('.item[data-text-pending]').length > 0) {
    
                var currentObject = $('#commands-dictionary').find('.item[data-text-pending]').eq(0);
                var currentText = currentObject.attr('data-command-activation');
                var writtedText = currentObject.attr('data-command-writted');
    
                var currentInputLine = $('.input-container p.input-line:last-of-type');
            
                var nextChar = currentText.charAt(writtedText.length);
    
                if (!currentObject.hasAttr('data-command-add-pause')) {
                    if(nextChar == ' ') {
                        currentInputLine.text(currentInputLine.text() + '\xa0');
                    } else {
                        currentInputLine.text(currentInputLine.text() + nextChar);
                    }
                    currentInputLine.addClass('content-line');
                }
    
                if (currentObject.hasAttr('data-start-simulate-ssh-connection')) {
                    window.isSshConnection = true;
                }
    
                if (currentObject.hasAttr('data-end-simulate-ssh-connection')) {
                    window.isSshConnection = false;
                }

                if (currentObject.hasAttr('data-command-step-lib-final-hack')) {
                    window.accessSemiAlertAudio.pause();
                    window.accessAlertAudio.play();
                }

                if (currentObject.hasAttr('data-hack-sequence-is-completed')) {
                    postVaultRoomSessionStatusIsHacked();

                    setTimeout(function(){
                        postVaultRoomSessionStatusIsHacked();
                    }, 300);
                }
    
                currentObject.attr('data-command-writted', currentObject.attr('data-command-writted') + nextChar);
    
                if (currentText == writtedText && window.isNewCommand == true && currentObject.find('.step').length > 0) {
                    $('.output-container').find('p.output-line').remove();
                    window.isNewCommand = false;
                }
    
                if (currentText === writtedText && currentObject.find('.step[data-step-pending]').length > 0) {
    
                    $('.output-container').append('<p class="output-line" style="display:none;"></p>');
    
                    var outputLine = $('.output-container .output-line:last-of-type');
    
                    var currentStep = currentObject.find('.step[data-step-pending]').eq(0);
                    var currentStepText = currentStep.text();
    
                    if (currentStep.hasClass('error')) {
                        outputLine.addClass('error');
                    }
    
                    outputLine.append(currentStepText);
                    outputLine.fadeIn(250);
                    currentStep.removeAttr('data-step-pending');

                    $(".output-container").animate({ scrollTop: $('.output-container').prop("scrollHeight")}, 50);
                    playBeepSound();
    
                } else if (currentText === writtedText) {
    
                    var nextCommandLine = currentObject.siblings('[data-text-pending]').eq(0);
                    if (nextCommandLine.length > 0) {
                        //if top container has empty last of type input p.input-line, use it before create new
                        if ($('.input-container').find('p.input-line:last-of-type').text().trim() !== '') {
                            $('.input-container').append('<p class="input-line content-line"></p>');
                            if (window.isSshConnection) {
                                $('.input-container').find('p.input-line:last-of-type').addClass('ssh-connection');
                            }
                        }
                    }
                    
                    $(".input-container").animate({ scrollTop: $('.input-container').prop("scrollHeight")}, 100);
    
    
                    currentObject.removeAttr('data-text-pending');
                    window.isNewCommand = true;
                }
    
            } else {
    
                clearInterval(typpingInterval);
                playFinishAlertSound();
                window.accessAlertAudio.pause();
            }
    
            window.tippyngTimeInterval = calculateRandomNumber(30, 100);
        }, window.tippyngTimeInterval);
    }

    function vaultSystemHackSequenceIsCompleted() {
    
        postVaultRoomSessionStatusIsHacked();

    }
})