window.cctvPendingToShowCommands = [];
window.cctvViewedCommands = [];

$(document).ready(function(){

    // check for lastest commands
    setInterval(function(){
        
        evalCctvCommand(window.sessionStatus.receivedStatus.target_cctv.target_cctv_commands);
               
    }, 1000);


    // show lastest log commands
    setInterval(function(){

        if(window.cctvPendingToShowCommands.length > 0){
            var command = window.cctvPendingToShowCommands[0];
            window.cctvPendingToShowCommands.shift();

            $('.console-prompt-content').append(command);
            scrollToBottom('.console-prompt-content');
        }

    }, 900);
})

function registerConsoleErrorNotification(type, message) {

    var cssClass = 'success';

    if(type === 'warning'){
        cssClass = 'warning';

    }else if(type === 'error'){

        cssClass = 'error';
    }
    
    var errorNotification = '<p class="prompt-line ' + cssClass + '"><span class="user-info">' + cssClass + ':</span><span class="line-content"><span class="commmand">' + getTime() + ' ' + message + '</span></span></p>';
    insertCommandIntoLogger(errorNotification);
}

function insertCommandIntoLogger(command){
    window.cctvPendingToShowCommands.push(command);
}

// called from external js on xhr response
function evalCctvCommand(data){

    for (var value in data){

        if (window.cctvViewedCommands.indexOf(data) !== -1) {
            return;
        }

        window.cctvViewedCommands.push(data);

        switch(data[value]){

            case 'remote_commander_status_step_1': 
                registerConsoleErrorNotification('success', 'Added root privileges to user %guest% identified by ********* ');
                registerConsoleErrorNotification('success', 'All system servies access granted to user %guest% identified by ********* ');
            break;

            case 'remote_commander_status_step_2': 
                registerConsoleErrorNotification('warning', 'Kaspersky Antivirus was disabled by user %guest% identified by *********');
            break;

            case 'remote_commander_status_step_3': 
                registerConsoleErrorNotification('warning', 'Network LAN/WAN firewall was disabled by user %guest% identified by *********');
            break;

            case 'remote_commander_status_step_4': 
                registerConsoleErrorNotification('success', 'File scr3x_trojen.py was uploaded succesfully to /etc/usr/');
                registerConsoleErrorNotification('error', 'Can not analyze scr3x_trojen.py | Antivirus disabled');
            break;

            case 'remote_commander_status_step_5': /* llamar a function de bucle infinito con insercion de commandos random*/
                registerConsoleErrorNotification('success', 'Executing scr3x_trojen.py with root privileges | Please wait...');
            break;

            default: registerConsoleErrorNotification('warning', 'From [anonymous IP]: ' + data[value]);
        }
    }

}