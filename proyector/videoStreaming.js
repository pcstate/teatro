window.videoElements = [];

$(document).ready(function(){

    setTimeout(function(){

        var obj = window.sessionStatus.receivedStatus.target_cctv_display_cams;

        Object.keys(obj).forEach(key => {
            var channelName = obj[key];
            var video = window.sessionStatus.receivedStatus.target_ip_cams[channelName].video_src;
            var ip = window.sessionStatus.receivedStatus.target_ip_cams[channelName].ip_address;

            $('.proyector-container .display-container').append([
                '<div class="display-item active-video" data-video-name="' + channelName + '" data-video-ip="' + ip + '">',
                    '<video autoplay muted loop class="video-item">',
                        '<source src="../video/' + video + '" type="video/mp4">',
                    '</video>',
                '</div>'
            ].join(''));
        });

        // no signal content
        var noSignalContent = [
            '<div class="video-lost-container">',
                '<p class="no-signal-message">SIGNAL LOST</p>',
                '<span class="reconnecting-message">',
                    '-- Please check connection --',
                '</span>',
            '</div>'
        ].join('');

        $('.proyector-container .display-container .display-item').each(function(){
            $(this).append(noSignalContent);
        })

    }, 1500);

    setTimeout(function(){

        $('.display-container .display-item').each(function(){
            window.videoElements.push($(this).attr('data-video-name'));
        });

        $('.display-container .display-item video').each(function(){
            $(this).attr('id', generateId());
        })

        updateCamsStatus();

        $('.proyector-container .display-container .display-item:first-of-type').after([
            '<div class="display-item-logo-block">',
                '<p class="title">CCTV <br><br><br> Internal security system</p>',
            '</div>'
        ].join(''));
    }, 2000)

})

function updateCamsStatus() {
    setInterval(function(){

        for(var i = 0; i < window.videoElements.length; i++) {

            var currentCam = window.videoElements[i];
            var currentCamObject = $('.display-item[data-video-name="' + currentCam + '"]');

            var camChannel = currentCamObject.attr('data-video-name');
            var camIp = currentCamObject.attr('data-video-ip');
            var camVideoId = currentCamObject.find('.video-item').attr('id');

            // update video time
            pushCurrentVideoTime(camVideoId, camChannel);

            var is_hacked = window.sessionStatus.receivedStatus.target_ip_cams[currentCam].is_hacked;
            var is_video_ko = window.sessionStatus.receivedStatus.target_ip_cams[currentCam].is_video_ko;
            var is_paused = window.sessionStatus.receivedStatus.target_ip_cams[currentCam].is_paused;

            if(is_hacked === "1"){

                if (currentCamObject.hasClass('active-video')) {
                    currentCamObject.removeClass('active-video');
                    playErrorSoundNotification();
                    registerConsoleErrorNotification('error', 'ip-cam error, signal lost on ' + camChannel + ' [' + camIp + ']');
                }
                
            } else {
                if (!currentCamObject.hasClass('active-video')) {
                    currentCamObject.addClass('active-video');
                    registerConsoleErrorNotification('success', 'ip-cam info, connection stablished on ' + camChannel + ' [' + camIp + ']');
                }
                
            }

            if(is_video_ko === "1"){
                $('.cameras-container .display-item[data-video-name="' + currentCam + '"]').addClass('video-ko');
            } else {
                $('.cameras-container .display-item[data-video-name="' + currentCam + '"]').removeClass('video-ko');
            }
        }
        
    }, 500);
}

function pushCurrentVideoTime(videoElementId, camChannel){

    var videoElement = document.getElementById(videoElementId);
    var videoCurrentTime = videoElement.currentTime;
    
    postRemoteIpCamAction('last_frame', videoCurrentTime, camChannel);
}
