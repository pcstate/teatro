$(document).ready(function(){

    // create new db session:
    createNewSession();

    // just blyat
    registerConsoleErrorNotification('success', 'System memory Ok! | 4096 Megabytes available');
    // then 
    setTimeout(function(){
        registerConsoleErrorNotification('warning', 'Added refuse connection rule into firewall | From: 84.153.12.75 (RU) Cause: Too much login attempts failed');
    }, 13600);

    $('.proyector-screen-saver .confirm-btn').on('click', function(){
        $('.proyector-screen-saver').addClass('hidden');
    })
    
})

window.soundActive = false;

window.errorCamNotificationAudio = new Audio('../audio/cam_error_beep.wav');
window.errorCamShadowAudio = new Audio('../audio/shadowSound.wav');

window.errorCamShadowAudio.volume = 20/100;

window.errorCamShadowAudio.addEventListener('ended', function(){
    window.soundActive = false;
});

function playErrorSoundNotification(){
    if (!window.soundActive) {
        window.soundActive = true;

        window.errorCamNotificationAudio.play();
        window.errorCamShadowAudio.play();

    } 
}
