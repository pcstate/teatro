var player = document.getElementById("police-audio-player");
var audio = document.getElementById("audiopolicia");
var canvas = document.getElementById("audio-spectrum");

var o = {
    "wave": {
        wave: true,
        colors: ["rgb(0, 255, 255)", "rgba(0, 255, 255,.2)"]
    },
    "bars":{
        bars:true,
        colors:["#f32a66","#f55c57","#f7a942","#f8e82f"],
        stroke:4
    },
    "bars_blocks":{
        bars_blocks:true,
        colors:["#f604a3","#f604a333"]
    },
    "dualbars":{
        dualbars:true,
        colors:["#09ff00","#09ff00","#09ff00","#09ff00"]
    },
    "orbs":{
        orbs:true,
        colors:["rgba(68, 8, 247,.4)","rgba(68, 8, 247,1)"]
    },
    "dualbars_blocks":{
        dualbars_blocks:true,
        colors:["#09ff00","#09ff0007"]
    },
    "round_wave":{
        round_wave:true,
        colors:["#f86300","#f86300"]
    },
    "shine":{
        shine:true,
        colors:["#fff","#111"]
    },
    "ring":{
        ring:true,
        colors:["#ff1600","#fe522e44"]
    },
    "flower":{
        flower:true,
        colors:["#ac3c7c","#c1653c","#aac340","#45bb60","#429f96","#459cbe","#2ab478","#accf2e","#ca913e","#cb4e54"]
    },
    "flower_blocks":{
        flower_blocks:true,
        colors:["#80f708","#80f70833"]
    },
    "star":{
        star:true,
        colors:["#f70880","#f7088044","#f8305c"]
    }
}

canvas.height = $("#audio-spectrum").height();
canvas.width = $("#audio-spectrum").width();
window.audioIsHacking = false;

var newAudio = document.getElementById("audiopolicia");

var W = new Wave();
W.fromElement("audiopolicia", "audio-spectrum", o.dualbars);


$(function () {

    window.radioTransmissionIsHacked = false;

    window.radioTransmissionHackedCheckInterval = setInterval(function(){
        
        getRadioTransmissionIsHacked();

        if(window.radioTransmissionIsHacked){
            clearInterval(window.radioTransmissionHackedCheckInterval);
            //click
            $('.police-break-transmission').click();
        }

    }, 1500);

    newAudio.addEventListener("ended", function () {
        registerConsoleErrorNotification('error', 'Radio transmission signal lost');
    })

    $('.police-break-transmission').on('click', function(){
        audio.pause();

        audio = null;
        W = null;

        $('#audiopolicia').attr('src', '../audio/cortopolicia.mp3');

        newAudio.play();
        window.audioIsHacking = true;

        registerConsoleErrorNotification('warning', 'The radio transmission signal is too weak or too saturated');

        $(this).addClass('hacked');
    })
    
    $('#police-audio-player').on('click', function(){
        
        if($(this).hasClass('playing')) {
            
            //stop
            $(this).removeClass('playing');

            if(window.audioIsHacking) {
                newAudio.pause();
            }else {
                audio.pause();
            }
            
        } else {
                        
            if(window.audioIsHacking) {
                newAudio.play();
            }else {
                audio.play();
            }

            $(this).addClass('playing');
            
        }
    })
    
})

