$(document).ready(function(){

    $('[data-explorer-item].path').each(function(index){
        $(this).attr('data-explorer-item-id', generateId());
        $(this).attr('data-explorer-history-next-id', '');
    })

    $('[data-shortcut-for="file-explorer"]').on('dblclick', function(){

        //set cursor (.active) on root inside specified path (data-folder-link)
        $('#file-explorer-wrapper .path.active').removeClass('active');
        $('#file-explorer-wrapper .path').removeClass('parent');
        $('#file-explorer-wrapper').find('.path.root').addClass('active');

        // empty history when opening app
        window.explorerPathHistory = [$('.path.active[data-explorer-item]').attr('data-explorer-item-id')];
        window.explorerPathHistoryIndex = 0;
        window.explorerForwardPathHistory = [];

        $('.history-controls-container .item').addClass('disabled');

        updateExplorerPathNavigator();
    })

    $('.history-controls-container .item').on('click', function(){

        if($(this).hasClass('disabled')){
            return;
        }

        if($(this).hasClass('backward')){
            moveBackwardInPathHistory();
        }

        if($(this).hasClass('forward')){
            moveForwardInPathHistory();
        }
    })

    // double click on path item
    $('#file-explorer-wrapper [data-explorer-item]').on('dblclick', function(){

        if($(this).hasClass('path') && !$(this).hasClass('active') && !$(this).hasClass('parent')){

            // simulate to open path
            var toOpenId = $(this).attr('data-explorer-item-id');

            //$element target
            $targetPath = $('[data-explorer-item][data-explorer-item-id="' + toOpenId + '"]');

            // check if parent of this path have reference to this path or another path (or empty)
            var closestParentId = $targetPath.closest('.path.active[data-explorer-item]').attr('data-explorer-item-id');
            var closestParentReferenceNextPath = $('.path.active[data-explorer-item-id="' + closestParentId + '"]').attr('data-explorer-history-next-id');

            //Add parent path to backward history
            addPathToBackwardHistory(toOpenId);

            if(closestParentReferenceNextPath === ''){
                $('.path[data-explorer-item-id="' + closestParentId + '"]').attr('data-explorer-history-next-id', toOpenId);
            
            }else if(closestParentReferenceNextPath !== toOpenId){

                // that means we have to remove completly forward history (user has open another path which not indicated by parent next ref)
                wipePathForwardHistory();
                $('.path[data-explorer-item-id="' + closestParentId + '"]').attr('data-explorer-history-next-id', toOpenId);
            }

            // finally move to target path
            moveToPath(toOpenId);
        }

    })
})

function moveToPath(pathId){

    // remove class parent to prevent click propagation for top level items
    $('#file-explorer-wrapper div.parent').removeClass('parent');
    $('#file-explorer-wrapper div.active').removeClass('active');

    // locate the target to 'open'
    $targetPath = $('[data-explorer-item][data-explorer-item-id="' + pathId + '"]');

    // animate elements display with fadeIn
    $targetPath.addClass('active');

    // This shit add 'display : block' style to child elements so flex cant works
    $targetPath.find('[data-explorer-item]').hide();
    $targetPath.find('[data-explorer-item]').fadeIn(200);

    //Parents of new active item:
    $targetPath.parents('[data-explorer-item].path').addClass('parent');

    updateExplorerPathNavigator();

}

function getPathNameById(id){
    return $('[data-explorer-item-id="' + id + '"]').attr('data-explorer-item-name');
}

function showHistoryBreadCrumbs(element, maxIndex){
    var breadcrumbs = 'File Explorer - ';
    maxIndex = maxIndex === false ? element.length - 1 : maxIndex;
    for (var i = 0; i <= maxIndex ; i++) {
        breadcrumbs += getPathNameById(element[i]) + ' / ';
    }
    return breadcrumbs;
}


function addPathToBackwardHistory(pathId){

    if(window.explorerForwardPathHistory.includes(pathId)){
        window.explorerForwardPathHistory = arrayRemove(window.explorerForwardPathHistory, pathId);
    }

    if(!window.explorerPathHistory.includes(pathId)){
        window.explorerPathHistory.push(pathId);
    }
    window.explorerPathHistoryIndex = window.explorerPathHistory.length - 1;
}

function wipePathForwardHistory(){
    window.explorerForwardPathHistory = [];
}

function addPathToForwardHistory(pathId){
    if(!window.explorerForwardPathHistory.includes(pathId)){
        window.explorerForwardPathHistory.unshift(pathId);
    }
}

// move one position in backward history
function moveBackwardInPathHistory(){
    if(window.explorerPathHistoryIndex >= 1) {

        //Remove last value from history and add it into forward history
        var currentPath = window.explorerPathHistory[window.explorerPathHistoryIndex];
        addPathToForwardHistory(currentPath);
        window.explorerPathHistory = arrayRemove(window.explorerPathHistory, currentPath);

        //Then move to index : path
        window.explorerPathHistoryIndex--;
        moveToPath(window.explorerPathHistory[window.explorerPathHistoryIndex]);
    }
    
}

// move one position in forward history
function moveForwardInPathHistory(){
    if(window.explorerForwardPathHistory.length > 0){

        var nextPath = explorerForwardPathHistory[0];
        addPathToBackwardHistory(nextPath);
        window.explorerForwardPathHistory = arrayRemove(window.explorerForwardPathHistory, nextPath);

        moveToPath(nextPath);
    }
}

function updateExplorerPathNavigator(){

    //Window title 
    var breadcrumbs = showHistoryBreadCrumbs(window.explorerPathHistory, window.explorerPathHistoryIndex);
    $('.window-container[data-application-id="file-explorer"]').find('.window-header-content .window-title').text(breadcrumbs);

    // if more than 1 paths in the history to navigate backward
    if(window.explorerPathHistoryIndex >= 1){
        $('#file-explorer-wrapper .history-controls-container .item.backward').removeClass('disabled');
    }else{
        $('#file-explorer-wrapper .history-controls-container .item.backward').addClass('disabled');
    }

    // if more than 0 paths in the history to navigate
    if(window.explorerForwardPathHistory.length > 0){
        $('#file-explorer-wrapper .history-controls-container .item.forward').removeClass('disabled');
    }else{
        $('#file-explorer-wrapper .history-controls-container .item.forward').addClass('disabled');
    }

    //Footer info:
    var pathsLength = $('#file-explorer-wrapper .path.active').children('.path').length;
    var filesLength = $('#file-explorer-wrapper .path.active').children('.file').length;

    var text = '';

    if(pathsLength > 0){
        text += pathsLength + ' folders ';
    }

    if(filesLength > 0){
        text += filesLength + ' files ';
    }
    $('#file-explorer-wrapper .footer-container .info').hide();
    $('#file-explorer-wrapper .footer-container .info').fadeIn(200);
    $('#file-explorer-wrapper .footer-container .info').text(text);
    
}

// login del marmot radio y empezar con cosas de servidor para interconectar los apps, ya habra tiempo de meter cosas chulas ...
// hay que terminar con el ip cctv remote comander (faltan botones etc) con xhr