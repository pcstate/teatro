window.clipBoardPreCopyValue = '';
window.targetPasteableElement = null;

$(document).ready(function(){

    $('.client-desktop').on('click', function(e){
        if($('#context-menu').is(':visible')) {
            if(!$('#context-menu').has(e.target).length > 0 && !$("#context-menu").is(e.target)){
                $('#context-menu').hide();
            }
        }
    })

    $('.selectionable').on('contextmenu', function(e){

        window.clipBoardPreCopyValue = getSelectionText();

        if(window.clipBoardPreCopyValue.trim() !== ''){
             /*show context menu*/
            var positionLeft = e.clientX;
            var positionTop = e.clientY;

            $('#context-menu').css({'left' : positionLeft + 'px', 'top' : positionTop + 'px'});
            $('#context-menu .option.copy').show();
            $('#context-menu .option.paste').hide();
            $('#context-menu').show();
        } 
    })

    $('.pasteable').on('contextmenu', function(e){
        var positionLeft = e.clientX;
        var positionTop = e.clientY;

        $('#context-menu').css({'left' : positionLeft + 'px', 'top' : positionTop + 'px'});
        $('#context-menu .option.copy').hide();
        $('#context-menu .option.paste').show();

        if(window.clipBoard.length > 0){

            //prepare paste values container
            positionLeft = positionLeft + $('#context-menu').outerWidth();
            $('#context-menu-paste-values').css({'left' : positionLeft + 'px', 'top' : positionTop + 'px'});

            // update pasteble items list
            $('#context-menu .paste-items-container').html(updatePasteableItemsList());

            // then add event listener to the new dom items
            $('#context-menu .option.paste [data-pasteable-option]').on('click', function(){
                var content = $(this).text();
                window.targetPasteableElement.val(content);
            })

            // show context menu
            $('#context-menu').show();

            window.targetPasteableElement = $(this);
        }
    })

    $('#context-menu .option.copy').on('click', function(){

        if($(this).hasClass('copy')){
            //if not empty selection
            if(window.clipBoardPreCopyValue.trim().length > 0) {
                
                //substr string, max 24 chars length
                var value = window.clipBoardPreCopyValue.substr(0, 24);

                // if clipboard contains it string remove it
                if(window.clipBoard.includes(value)){
                    window.clipBoard = arrayRemove(window.clipBoard, value);
                }

                //then insert the copied value
                window.clipBoard.unshift(value.trim());
            }

            // max of 5 paste items in paste submenu
            if(window.clipBoard.length > 5){
                window.clipBoard = arrayRemove(window.clipBoard, window.clipBoard[5]);
            }

            // clear clipboard precopy value
            window.clipBoardPreCopyValue = '';
            
        }
    })

    $('#context-menu .option').on('click', function(){
        $('#context-menu').hide();
    })
})

function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

function updatePasteableItemsList(){
    content = '';
    for(var i = 0; i < window.clipBoard.length; i++){
        content += '<div class="item" data-pasteable-option>' + window.clipBoard[i] + '</div>';
    }
    return content;
}