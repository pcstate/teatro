// prevent zoom scroll
$(document).keydown(function(e) {
    // 107 Num Key  +
    // 109 Num Key  -
    // 173 Min Key  hyphen/underscor Hey
    // 61 Plus key  +/= key
    if(e.ctrlKey == true && (e.which == '61' || e.which == '107' || e.which == '173' || e.which == '109'  || e.which == '187'  || e.which == '189')){
        e.preventDefault();
    }
});
    
$(window).on('wheel', function(e){
    if (e.ctrlKey == true) {
        //prevent wheel mouse scrolling?
        e.preventDefault();
    }
});

$(document).on('contextmenu', function(e){
    e.preventDefault();
})

$(document).ready(function(){

    $('body').css('filter', 'brightness(' + window.bodyBrightness + ')');

    $('.bottom-navigator-container .controls-area-container .brightness-control .range-control input').on('change', function(){
        var value = $(this).val() / 100;
        if(value <= 0.2){
            value = 0.2;
        }
        window.bodyBrightness = value;
        $('body').css('filter', 'brightness(' + window.bodyBrightness + ')');
    })
    
    //Hide main menu block if click event is not in them
    $('.client-desktop').on('click', function(e){

        // check for auto close items
        if(!$('.main-menu-container').hasClass('hidden')) {
            if(!$('.main-menu-container').has(e.target).length > 0 && !$(".main-menu-content").is(e.target) && !$('[data-toggle-main-menu]').is(e.target)) {
                $('.main-menu-container').addClass('hidden');
            }
        }

        if(!$('#calendar-container').hasClass('hidden')) {
            if(!$('#calendar-container').has(e.target).length > 0 && !$('.bottom-navigator-content .right-zone *').is(e.target)) {
                $('#calendar-container').addClass('hidden');
            }
        }

        if($('.bottom-navigator-container .controls-area-container .control-item').find('.range-control:not(hidden)').length > 0){
            if(!$('.bottom-navigator-container .controls-area-container .control-item').has(e.target).length > 0 && !$('.bottom-navigator-container .controls-area-container .control-item .range-control').has(e.target).length > 0 && !$('.bottom-navigator-container .controls-area-container .control-item .range-control').is(e.target)) {
                $('.bottom-navigator-container .controls-area-container .control-item .range-control').addClass('hidden');
            }
        }
    })

    // bottom control icons 

    $('.bottom-navigator-container .controls-area-container .control-item').on('click', function(e){
        if($(this).find('.range-control').hasClass('hidden')){
            $(this).find('.range-control').removeClass('hidden');
        }else{
            // allow user to use bottom control icon for hide range control
            if(!$(this).find('.range-control').has(e.target).length > 0 && !$(this).find('.range-control').is(e.target)){
                $(this).find('.range-control').addClass('hidden');
            }
            
        }

    })

    // create bottom calendar item (external lib)
    jsCalendar.new(document.getElementById('calendar-container'));

    $('.bottom-navigator-content .right-zone .clock-container').on('click', function(){
        $('#calendar-container').toggleClass('hidden');
    })


    // add process show interval to all windows
    $('[data-application-id]').attr('data-process-items-visible', 0);

    $('.main-action-button').on('keydown', function(e){
        if(e.which === 13){
            $(this).click();
        }
    })

    //Bottom menu display toggle:
    $('[data-toggle-main-menu]').on('click', function(){
        $('.main-menu-container').toggleClass('hidden');
    })

    //Navigator bar bottom Clock:
    setInterval(function(){
        var date = new Date();

        var hours = date.getHours();
        var minutes = date.getMinutes();

        if (hours < 10){
            hours = '0' + hours;
        }

        if (minutes < 10){
            minutes = '0' + minutes;
        }

        $('.clock-container .hours').text(hours);
        $('.clock-container .minutes').text(minutes);

    }, 1000);

})
