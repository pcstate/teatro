$(document).ready(function(){
    $('[data-application-id="net-tools-network-scan"]').find('.window-control.close').on('click', function(){
        if(typeof window.netToolsNetworkScanInterval !== 'undefined'){
            clearInterval(window.netToolsNetworkScanInterval);
        }
        decreaseCpuWidgetUssage(15);
    })
})

window.netToolsbeepAudio = new Audio('../audio/beep.wav');

function launchNetToolsNetworkScan(){

    //initialization of runtime wrapper xd
    $('#net-tools-network-scan-table-content').attr('data-items-counter', 0);
    $('#net-tools-network-scan-table-content').attr('data-items-max-count', 255);
    $('#net-tools-network-scan-table-content .item').hide();
    
    $('#net-tools-network-scan').find('.progress-container .progress-bar').css('width', '0%');
    $('#net-tools-network-scan').find('.progress-container .progress-bar').removeClass('completed');
    $('#net-tools-network-scan').find('.bottom-nav-container').show();
    $('#net-tools-network-scan').find('.bottom-nav-container .progress-text').show();

    // remove finished status
    $('#net-tools-network-scan').closest('.window-container').attr('application-status', '');
    $('#net-tools-network-scan').find('#net-tools-network-scan-table-content .item.target').removeClass('target-found');

    $('#net-tools-network-scan .bottom-nav-container .filter-input-container .filter-box .item').remove();

    var item = '<li class="item disabled" data-network-scan-filter-for="*">Show all</li>';
    $('#net-tools-network-scan .bottom-nav-container .filter-input-container .filter-box .filter-items').append(item);
    
    increaseCpuWidgetUssage(15);
    // here is where all start to flow::
    window.netToolsNetworkScanInterval = setInterval(function(){

        var totalItems = $('#net-tools-network-scan-table-content').attr('data-items-max-count');
        var currentItemIndex = parseInt($('#net-tools-network-scan-table-content').attr('data-items-counter'));
        if(currentItemIndex <= totalItems){

            $('#net-tools-network-scan-table-content .item').each(function(index){
                var targetText = '172.16.10.' + currentItemIndex;
                if($(this).find('td.ip').text().trim() === targetText){
                    $(this).fadeIn(300);

                    // play audio
                    window.netToolsbeepAudio.play();

                    var deviceType = $(this).find('td.type').text().trim();
                    
                    if($('#net-tools-network-scan .bottom-nav-container .filter-input-container .filter-box .item[data-network-scan-filter-for="' + deviceType + '"]').length === 0) {
                        var item = '<li class="item disabled" data-network-scan-filter-for="' + deviceType + '">' + deviceType + '</li>';
                        $('#net-tools-network-scan .bottom-nav-container .filter-input-container .filter-box .filter-items').append(item);
                    }
                }
            })

            var progress = Math.floor(calculateProgressPercent(totalItems, currentItemIndex));
            $('#net-tools-network-scan').find('.progress-container .progress-bar').css('width', progress + '%');
            $('#net-tools-network-scan').find('.progress-container .progress-text .percent').text(progress);
            
            $('#net-tools-network-scan-table-content').attr('data-items-counter', currentItemIndex + 1);

        }else{
            
            //add filter event
            $('#net-tools-network-scan .bottom-nav-container .filter-items .item').on('click', function(){
                var target = $(this).attr('data-network-scan-filter-for');

                $('#net-tools-network-scan-table-content tr.item').show();
                if(target === '*'){
                    return;
                }
                
                $('#net-tools-network-scan-table-content tr.item').each(function(index){
                
                    if($(this).find('td.type').text().trim() !== target){
                        $(this).hide();
                    }
                })

            })

            decreaseCpuWidgetUssage(15);

            $('#net-tools-network-scan .bottom-nav-container .filter-input-container .filter-box .item').removeClass('disabled');
            $('#net-tools-network-scan').find('.progress-container .progress-bar').addClass('completed');
            $('#net-tools-network-scan').find('.bottom-nav-container .progress-text').hide();
            clearInterval(window.netToolsNetworkScanInterval);

            // update application status
            $('#net-tools-network-scan').closest('.window-container').attr('application-status', 'completed');
            
        }
    }, 25);
}