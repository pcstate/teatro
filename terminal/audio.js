$(document).ready(function(){

    window.audioGeneralVolume = 100 / 100;
    // update volume with input range control

    $('.bottom-navigator-content .control-item.volume-control input[type="range"]').on('change', function(){
        window.audioGeneralVolume = parseInt($(this).val()) / 100;
        console.log(window.audioGeneralVolume);
    })

    // general click sound
    $('button, input, a').on('click', function(){
        playAudioClick();
    })

    $('.sound-btn').on('dblclick', function(){
        playAudioClick();
    })
})

function playAudioClick() {
    if (typeof window.audioClick === 'undefined') {
        window.audioClick = new Audio('../audio/click.wav');
    }

    window.audioClick.volume = window.audioGeneralVolume;
    window.audioClick.play();
}

function playAudioError() {

    if (typeof window.audioError === 'undefined') {
        window.audioError = new Audio('../audio/error.mp3');
    }

    window.audioError.volume = window.audioGeneralVolume;
    window.audioError.play();
}

function playAudioLoginSuccess() {

    if (typeof window.audioSuccesLogin === 'undefined') {
        window.audioSuccesLogin = new Audio('../audio/start-up.mp3');
    }

    window.audioSuccesLogin.volume = window.audioGeneralVolume;
    window.audioSuccesLogin.play();
}

function playAudioNotification() {

    if (typeof window.audioNotification === 'undefined') {
        window.audioNotification = new Audio('../audio/notification.mp3');
    }

    window.audioNotification.volume = window.audioGeneralVolume;
    window.audioNotification.play();
}