$(document).ready(function(){
    $('.client-os .client-os-wrapper .client-desktop-background').addClass('logo-presentation');
})

$('#button-login-action').on('click', function(e){
    
    e.preventDefault();
    $(this).focus();

    $('#login-profile-password').removeClass('error');
    $('#login-profile-password').removeClass('success');

    $('.login-full-container .login-container .form .loading-gif-container').show();

    setTimeout(function(){

        if($('#login-profile-username').val().trim() === window.loginUsername && $('#login-profile-password').val().trim() === window.loginPassword) {

            $('#login-profile-password').attr('class', 'success');
            $('.login-full-container').fadeOut(500);
            $('.client-os > .login-wrapper').addClass('hidden');

            playAudioLoginSuccess();

            setTimeout(function(){
                $('.client-os .client-os-wrapper .client-desktop-background').removeClass('logo-presentation');
            }, 1000);
            
            setTimeout(function(){
                $('[data-shortcut-for="autostart"]').dblclick();
            }, 3000);
            
        }else{

            $('#login-profile-password').val('');
            $('#login-profile-password').attr('class', 'error');
            $('#login-profile-password').focus();
            $('#login-profile-password').effect( 'shake', [], 500 );

            playAudioError();
        }

        $('.login-full-container .login-container .form .loading-gif-container').hide();
    }, 1000);
})


$('#main-menu-lock-screen-button').on('click', function(){

    if(!$('.main-menu-container').hasClass('hidden')){
        $('.main-menu-container').addClass('hidden');
    }
    
    $('#login-profile-password').val('');

    // show login lock button and hide login form
    $('#button-unlock-action').show();
    $('#login-profile-username').hide().addClass('hidden');
    $('#login-profile-password').hide().addClass('hidden');
    $('#button-login-action').hide().addClass('hidden');


    $('.client-os > .login-wrapper').removeClass('hidden');
    
    setTimeout(function(){
        $('.login-full-container').fadeIn(500);
        $('#login-profile-password').focus();
        $('.client-os .client-os-wrapper .client-desktop-background').addClass('logo-presentation');
    }, 600)

})

$('#button-unlock-action').on('click', function(e){
    e.preventDefault();

    $(this).fadeOut('fast', function(){
        $(this).addClass('hidden');

        $('#login-profile-username').val(window.sessionStatus.receivedStatus.user_config.login_username);
        $('#login-profile-username').fadeIn().removeClass('hidden');
        $('#login-profile-password').fadeIn().removeClass('hidden');
        $('#button-login-action').fadeIn().removeClass('hidden');

        $('#login-profile-password').focus();

    })
})

//just fo developmnt:
// $('.login-wrapper.login-gradient-bg.top').addClass('hidden');
// $('.login-wrapper.login-gradient-bg.bottom').addClass('hidden');
// $('.login-full-container').fadeOut(10);
