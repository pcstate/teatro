$(document).ready(function(){
    
    $('[data-application-id="radio-brocker"]').find('.window-control.close').on('click', function(){
        
        clearInterval(window.radioBrockerInterval);
        clearInterval(window.radioBrockerScaningInterval);

        decreaseCpuWidgetUssage(20);
    })

    // eventListeners in step 0: 
    $('#radio-brocker-wrapper').find('#start-scan-button').on('click', function(){
        initMarmotRadioBrockerLoadingLayout('list-targets');
    })

    // eventListeners in step 2:
    $('#radio-brocker-wrapper .list-targets-wrapper .radio-transmissor-list .item').on('click', function(){
        if($(this).hasClass('target')){
            $(this).removeClass('target');
            $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').addClass('disabled');
        }else{
            $(this).addClass('target');
            $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').removeClass('disabled');
        }
    })

    $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').on('click', function(){
        if(!$(this).hasClass('disabled')){
            initMarmotRadioBrockerLoadingLayout('start-attack');
        }
    })

    // on re-scan button if theres no available items to atack in main targets list
    $('#radio-brocker-wrapper .list-targets-wrapper .error-not-found .rescan').on('click', function(){
        initMarmotRadioBrockerLoadingLayout('list-targets');
    })
})

// hide all layouts
function hideAllRadioBrockerSteps(){
    $('#radio-brocker-wrapper').find('.pre-execution-wrapper').removeClass('hidden');
    $('#radio-brocker-wrapper').find('.start-scan-wrapper').addClass('hidden');
    $('#radio-brocker-wrapper').find('.searching-wrapper').addClass('hidden');
    $('#radio-brocker-wrapper').find('.list-targets-wrapper').addClass('hidden');
    $('#radio-brocker-wrapper').find('.execution-wrapper').addClass('hidden');
}

//step 0 : main screen: (remove the other screens)
function initMarmotRadioBrockerLoginScreen(){
    hideAllRadioBrockerSteps();
    $('#radio-brocker-wrapper').find('.start-scan-wrapper').removeClass('hidden');
}

//setp 1 : scaning radio transmissors
function initMarmotRadioBrockerLoadingLayout(nextStep){

    window.radioBrockercaningPercentage = 0;
    var canvas = document.getElementById('radio-brocker-scaning');
    var radius = canvas.width / 3;
    var intervalTime = 125;
    var message = 'Scanning';
    var nextStepTimeout = 3000;

    clearCanvas(canvas);
    hideAllRadioBrockerSteps();

    if(nextStep === 'start-attack' || nextStep === 'global-sequencer-start-attack'){
        intervalTime = 50;
        message = 'Initializing';
        nextStepTimeout = 1000;
    }

    $('#radio-brocker-wrapper .searching-wrapper .percentage-text .percentage').text('0');

    $('#radio-brocker-wrapper').find('.searching-wrapper').removeClass('hidden');
    $('#radio-brocker-wrapper .searching-wrapper .searching-text').text(message);
    $('#radio-brocker-wrapper .searching-wrapper .searching-text').removeClass('finally');


    window.radioBrockerScaningInterval = setInterval(function(){
        
        if(window.radioBrockercaningPercentage <= 100) {

            drawPercentageCircle(window.radioBrockercaningPercentage, radius, canvas);
            $('#radio-brocker-wrapper .searching-wrapper .percentage-text .percentage').text(window.radioBrockercaningPercentage);

            window.radioBrockercaningPercentage++;
        }else {
            clearInterval(window.radioBrockerScaningInterval);
            // call next function (list transmissors)
            $('#radio-brocker-wrapper .searching-wrapper .searching-text').addClass('finally');
            $('#radio-brocker-wrapper .searching-wrapper .searching-text').text('Wait please');
            

            setTimeout(function(){
                if(nextStep === 'start-attack'){
                    //check for last time if can hack and reserve attack in db
                    if(!window.radioBrockerIsHackedOnDb){
                        initMarmotRadioBrocker();
                    }else{
                        //console.log('error ocurred, cant find radio transmissor');
                    }
                }else if(nextStep === 'global-sequencer-start-attack') {
                    initMarmotRadioBrocker();
                } else {
                    initMarmotRadioBrockerListTargets();
                }
                
            }, nextStepTimeout);

        }
    }, intervalTime);
}

// step 2 select target to atack (clear and restore all *)
function initMarmotRadioBrockerListTargets(){
    hideAllRadioBrockerSteps();

    // remove target class from * list items
    $('#radio-brocker-wrapper .list-targets-wrapper .radio-transmissor-list .item').removeClass('target');

    if(window.remoteRadioTransmissorIsActive !== true){
        $('#radio-brocker-wrapper .list-targets-wrapper .radio-transmissor-list .item').hide();
        $('#radio-brocker-wrapper .list-targets-wrapper .error-not-found').show();
        $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').hide();
    }else{
        $('#radio-brocker-wrapper .list-targets-wrapper .radio-transmissor-list .item').show();
        $('#radio-brocker-wrapper .list-targets-wrapper .error-not-found').hide();
        $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').show();
    }

    //Blocking button
    $('#radio-brocker-wrapper .list-targets-wrapper #start-attack').addClass('disabled');
    
    //Show screen
    $('#radio-brocker-wrapper').find('.list-targets-wrapper').removeClass('hidden');
}

// step 3 hack process
function initMarmotRadioBrocker(){

    postRadioTransmissionIsOnHacking();

    hideAllRadioBrockerSteps();

    $('#radio-brocker-wrapper').find('.pre-execution-wrapper').addClass('hidden');
    $('#radio-brocker-wrapper').find('.execution-wrapper').removeClass('hidden');

    //radio-brocker-wrapper-hex-container
    $('#radio-brocker-wrapper-hex-container').html('');
    //radio-brocker-wrapper-asci-container
    $('#radio-brocker-wrapper-asci-container').html('');

    var hexTable = '<table>';
    for(var i = 1; i <= 10; i++) {
        hexTable += '<tr data-list-position="' + i + '">';
        for(var j = 1; j <= 10; j++) {
            var attr = j <= 5 ? 'data-group="1"' : 'data-group="2"';
            hexTable += '<td ' + attr + '></td>';
        }
        hexTable += '</tr>';
    }

    hexTable += '</table>';

    var asciiTable = '<table>';
    for(var i = 1; i <= 10; i++) {
        asciiTable += '<tr data-list-position="' + i + '">';
        for(var j = 1; j <= 2; j++) {
            var attr = j === 1 ? 'data-group="1"' : 'data-group="2"';
            asciiTable += '<td class="text-off" ' + attr + '></td>';
        }
        asciiTable += '</tr>';
    }

    asciiTable += '</table>';

    var audioSpectrContainer = '<div class="audio-spectre-container"></div>';

    $('#radio-brocker-wrapper-hex-container').html(hexTable);
    $('#radio-brocker-wrapper-asci-container').html(asciiTable + audioSpectrContainer);
    
    var audioBreakDuration = window.audioBrockerTimeDuration; // seconds
    var operationInterval = 75;
    var totalOperations = audioBreakDuration * 1000 / operationInterval;

    $('#radio-brocker-wrapper .execution-wrapper').attr('data-operation-count', 1);
    $('#radio-brocker-wrapper .execution-wrapper').attr('data-operation-total-count', totalOperations);
    $('#radio-brocker-wrapper-hex-container').attr('visible-tr-elements', 0);
    $('#radio-brocker-wrapper-asci-container tr td').text('');

    // reset finally messages
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper .flex-center-container .big-message-box').removeClass('completed');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper .flex-center-container .big-message-box .title').text('Please wait...');

    // shit ::
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-1');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-2');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-3');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-4');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-5');
    $('#radio-brocker-wrapper .execution-wrapper .body-wrapper').removeClass('step-6');
    

    increaseCpuWidgetUssage(20);
    
    window.radioBrockerInterval = setInterval(function(){
        
        var totalOperationCounter = parseInt($('#radio-brocker-wrapper .execution-wrapper').attr('data-operation-total-count'));
        var currentOperationCounter = parseInt($('#radio-brocker-wrapper .execution-wrapper').attr('data-operation-count'));
        var visibleTrElementCounter = parseInt($('#radio-brocker-wrapper-hex-container').attr('visible-tr-elements'));
        var totalVisibleTrElements = $('#radio-brocker-wrapper-hex-container table tr').length;

        if(currentOperationCounter <= totalOperationCounter){

            // Playing with imaginary hacking phases:
            var percent = calculateProgressPercent(totalOperationCounter, currentOperationCounter);
            var subPercent = 0;
            var message = '';
            var stepClass = '';

            // Phase 1 Decoding transmission data
            if (percent >= 0 && percent < 35) {
                subPercent = calculateProgressPercent(35, percent);
                message = 'Decoding transmission data';
                stepClass = 'step-1';
            }

            // Phase 2 Analyzing transmission data
            if (percent >= 35 && percent < 50) {
                if(!window.radioBrockerIsHackedOnDb){
                    postRadioTransmissionIsHacked();
                    window.radioBrockerIsHackedOnDb = true;
                }
            
                subPercent = calculateProgressPercent(5, percent - 35);
                message = 'Analyzing transmission data';
                stepClass = 'step-2';
            }

            // Injecting script into transmission
            if (percent >= 50 && percent < 75) {
                subPercent = calculateProgressPercent(25, percent - 50);
                message = 'Injecting fake audio data into transmission';
                stepClass = 'step-3';
            }

            // Code injected succesfully , Please wait..
            if (percent >= 75 && percent < 80) {
                subPercent = calculateProgressPercent(5, percent - 75);
                message = 'Injecting fake audio data into transmission';
                stepClass = 'step-4';
            }

            // Please wait while transmission error 'ocurs'
            if (percent >= 80 && percent < 100) {
                subPercent = calculateProgressPercent(20, percent - 80);
                message = 'Injecting fake audio data into transmission';
                stepClass = 'step-5';
            }

            // Transmission deny attack has been succesfully completed
            if (percent == 100) {
                subPercent = 100;
                message = 'Transmission deny attack has been succesfully completed';
                stepClass = 'step-6';

                //completed
                $('#radio-brocker-wrapper .execution-wrapper .body-wrapper .flex-center-container .big-message-box').addClass('completed');
                $('#radio-brocker-wrapper .execution-wrapper .body-wrapper .flex-center-container .big-message-box .title').text('Attack completed');
            }

            $('#radio-brocker-wrapper .body-wrapper').addClass(stepClass);

            var parsedPercent = Math.floor(subPercent);
            if(subPercent < 100) {
                $('#radio-brocker-wrapper .execution-wrapper .footer-wrapper .progress-bar-container').show();
                parsedPercent = parsedPercent + '%';
            }else{
                $('#radio-brocker-wrapper .execution-wrapper .footer-wrapper .progress-bar-container').hide();
                parsedPercent = '';
            }

            $('#radio-brocker-wrapper .execution-wrapper .footer-wrapper .progress-bar-container .progress-bar').css('width', subPercent + '%');
            $('#radio-brocker-wrapper .execution-wrapper .body-wrapper .flex-center-container .big-message-box .subtitle').text(message);
            $('#radio-brocker-wrapper .execution-wrapper .footer-wrapper .progress-bar-container .percent-text-container .percent').text(parsedPercent);

            // action here
            if(visibleTrElementCounter <= totalVisibleTrElements){

                $('#radio-brocker-wrapper-hex-container table tr').removeClass('current');
                $('#radio-brocker-wrapper-hex-container table tr:nth-child(' + visibleTrElementCounter + ')').addClass('matched');
                $('#radio-brocker-wrapper-hex-container table tr:nth-child(' + visibleTrElementCounter + ')').addClass('current');

                var rightSideLoc = $('#radio-brocker-wrapper-hex-container table tr:nth-child(' + visibleTrElementCounter + ')').attr('data-list-position');

                $('#radio-brocker-wrapper-asci-container tr[data-list-position="' + rightSideLoc + '"] td').removeClass('text-off');

                // each tr ok lo k
                $('#radio-brocker-wrapper-hex-container table tr.current td').each(function(index){
                    var randomHex = createHexRandomString(2);
                    var hexString = hex_to_ascii(randomHex);

                    var trItemCount = $(this).closest('tr').attr('data-list-position');
                    var tdItemCount = $(this).attr('data-group');

                    if(['00', 'AA', 'BB', 'CC', 'DD', 'EE', 'FF', '11', '22', '33', '44', '55', '66', '77', '88', '99', '28', 'DE', 'EF'].includes(randomHex)){
                        $(this).addClass('match');
                        $('#radio-brocker-wrapper-asci-container tr[data-list-position="' + trItemCount + '"] td[data-group="' + tdItemCount + '"]').addClass('match');
                    }

                    $(this).text(randomHex);

                    $('#radio-brocker-wrapper-asci-container tr[data-list-position="' + trItemCount + '"] td').each(function(){
                        var randomString = '';
                        for(var i = 0; i <= 20; i++){
                            if(i % 5 === 0){
                                randomString += ' ';
                            }else{
                                randomString += hex_to_ascii(createHexRandomString(2));
                            }
                        }
                        $(this).text(randomString);
                    })
                    
                })
                //increment next step
                visibleTrElementCounter++;
                $('#radio-brocker-wrapper-hex-container').attr('visible-tr-elements', visibleTrElementCounter);

            }else{

                //reset and go again
                $('#radio-brocker-wrapper-hex-container table tr').removeClass('matched');
                $('#radio-brocker-wrapper-hex-container table tr td').removeClass('match');
                $('#radio-brocker-wrapper-asci-container table tr td').removeClass('match');
                $('#radio-brocker-wrapper-hex-container table tr').removeClass('current');
                $('#radio-brocker-wrapper-hex-container').attr('visible-tr-elements', 0);

                $('#radio-brocker-wrapper-asci-container tr td').text('');
                $('#radio-brocker-wrapper-hex-container tr td').text('');
                $('#radio-brocker-wrapper-asci-container tr td').addClass('text-off');
                
            }

            currentOperationCounter++;
            $('#radio-brocker-wrapper .execution-wrapper').attr('data-operation-count', currentOperationCounter);
        }else{
            clearInterval(window.radioBrockerInterval);
            decreaseCpuWidgetUssage(20);
            $('#radio-brocker-wrapper').closest('.window-container').attr('application-status', 'completed');

        }

    }, operationInterval);
}