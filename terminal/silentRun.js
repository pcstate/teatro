$(document).ready(function() {

    $('[data-silent-run]').each(function(){
        var target = $(this).attr('data-silent-run');
        $('body').append('<div style="display:none" id="' + target + '"></div>');
    })

    // register silent run:
    $('#run-silent-password-decryptor').on('click', function(){
        //hackPassword.js
        launchPasswordDecryptorApplication();
    })
    
    $('#run-silent-password-unhasher').on('click', function(){
        //hackPassword.js
        launchPasswordUnhasher();
    })
    
    $('#run-silent-console-focus').on('click', function(){
        //commandPrompt.js
        launchConsoleFocus();
    })
    
    $('#run-silent-net-tools-network-scan').on('click', function(){
        // netTools.js
        launchNetToolsNetworkScan();
    })
    
    $('#run-silent-radio-brocker').on('click', function(){
        // radioBrocker.js
        initMarmotRadioBrockerLoginScreen();
    })

    
    $('#run-global-app-launcher').on('click', function(){
        runGlobalAppSequence();
    })
    
    $('#run-auto-login-apps').on('click', function(){
        $('[data-application-id="autostart"]').closeWindow();
        $('[data-shortcut-for="global-app-launcher"]').dblclick();
    })
})



        