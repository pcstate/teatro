$.fn.setCurrentTask = function(){
    $(this).siblings('.current').removeClass('current').removeClass('pending').addClass('done');
    $(this).removeClass('pending').addClass('current');
}

$.fn.setDoneTask = function(){
    $(this).siblings('.current').removeClass('current').removeClass('pending').addClass('done');
    $(this).removeClass('current').removeClass('pending').addClass('done');
}

window.globalsequencehakCctvCamarasInterval = null;
window.checkForCompletedStatusInterval = null;
window.globalsequenceCheckForReadyStatusInterval = null;
window.globalsequenceCheckForCompletedStatusInterval = null;
window.globalCheckForReadyStatusInterval = null;
window.globalSequenceCheckForFinishStatusInterval = null;

window.checkingIntervalTime = 250;

$(document).ready(function(){
    $('.window-container[data-application-id="global-app-launcher"]').find('.window-control.close').on('click', function(){
        cleanGlobalSequenceIntervals();
    })
})

function cleanGlobalSequenceIntervals(){
    clearInterval(window.globalsequencehakCctvCamarasInterval);
    clearInterval(window.checkForCompletedStatusInterval);
    clearInterval(window.globalsequenceCheckForReadyStatusInterval);
    clearInterval(window.globalsequenceCheckForCompletedStatusInterval);
    clearInterval(window.globalCheckForReadyStatusInterval);
    clearInterval(window.globalSequenceCheckForFinishStatusInterval);
}

function task(taskName) {
    return $('#global-app-launcher-wrapper .task-item[data-task-item-name="' + taskName + '"]');
}

function focusOnGlobalAppSequencer(){
    $('.window-container[data-application-id="global-app-launcher"]').autoWindowSetZindex();
}

function runGlobalAppSequence() {
    //preload 
    // update window position
    $('[data-application-id="global-app-launcher"]').animate({
        'left' : '20px',
        'top' : '20px',
    }, 0);

    $('#global-app-launcher-wrapper').find('.task-item').removeClass('done').removeClass('current').addClass('pending');

    // run first step
    task('scan-lan-cctv').setCurrentTask();

    setTimeout(function(){
        runGlobalSequenceStep1();
    }, window.checkingIntervalTime);
    
}

function runGlobalSequenceStep1() {
    // run app
    $('#net-tools-network-scan').find('.progress-container .progress-bar').css('width', '0px');
    $('[data-shortcut-for="net-tools-network-scan"]').dblclick();
    $('#net-tools-network-scan').find('#net-tools-network-scan-table-content .item.target').removeClass('target-found');

    // set window position to center of client screen
    $('#net-tools-network-scan').closest('.window-container').css('left', (($(window).width()/2) - 200 )+ 'px'); 

    window.globalSequenceCheckForFinishStatusInterval = setInterval(function(){
        if ($('#net-tools-network-scan').closest('.window-container').attr('application-status') === 'completed') {

            $('#net-tools-network-scan').find('#net-tools-network-scan-table-content .item.target').addClass('target-found');

            task('search-target-cctv').setCurrentTask();

            clearInterval(window.globalSequenceCheckForFinishStatusInterval);
            runGlobalSequenceStep2();
        }
    }, window.checkingIntervalTime);
}

function runGlobalSequenceStep2() {

    task('check-connection').setCurrentTask();

    $('[data-application-id="password-recovery"]').attr('application-status', '');
    var ipAddressArray = [172, 16, 10, 60];
    for (var i = 0; i < ipAddressArray.length; i++) {
        $('#main-password-recovery-wrapper .ip-address-form-container').find('.ip-addres-part-' + (i + 1)).val('');
        $('#main-password-recovery-wrapper .ip-address-form-container').find('.ip-addres-part-' + (i + 1)).addClass('error');
    }

    $('#main-password-recovery-wrapper .main-action-button').addClass('error');

    setTimeout(function(){
        $('[data-shortcut-for="password-recovery"]').dblclick();
    }, window.checkingIntervalTime);

    

    window.globalCheckForReadyStatusInterval = setInterval(function(){

        

        if ($('[data-application-id="password-recovery"]').attr('application-status') === 'ready') {
            clearInterval(window.globalCheckForReadyStatusInterval);
            runGlobalSequenceStep3();
        }
    }, window.checkingIntervalTime);
}

function runGlobalSequenceStep3() {
    var ipAddressArray = [172, 16, 10, 60];
    for (var i = 0; i < ipAddressArray.length; i++) {
        $('#main-password-recovery-wrapper .ip-address-form-container').find('.ip-addres-part-' + (i + 1)).val(ipAddressArray[i]);
        $('#main-password-recovery-wrapper .ip-address-form-container').find('.ip-addres-part-' + (i + 1)).removeClass('error');
    }

    $('#main-password-recovery-wrapper .main-action-button').removeClass('error');

    setTimeout(function(){
        $('#main-password-recovery-wrapper .main-action-button').click();
        runGlobalSequenceStep4();
    }, window.checkingIntervalTime);
}

function runGlobalSequenceStep4() {

    $('[data-application-id="password-unhasher"]').attr('application-status','');

    window.globalsequenceCheckForCompletedStatusInterval = setInterval(function(){
        if ($('[data-application-id="password-unhasher"]').attr('application-status') === 'completed') {

            focusOnGlobalAppSequencer();

            window.clipBoard[28] = $('.unhasher-result').text();
            task('save-remote-key').setDoneTask();
            task('clear-desktop').setCurrentTask();
            runGlobalSequenceStep5();

            clearInterval(window.globalsequenceCheckForCompletedStatusInterval);
        }
    }, window.checkingIntervalTime);   
}

function runGlobalSequenceStep5() {

    setTimeout(function(){
        $('.window-container:not(.focused-window)').closeWindow();
    }, window.checkingIntervalTime);

    setTimeout(function(){
        task('clear-desktop').setDoneTask();
        runGlobalSequenceStep6();
    }, 3000);
}

function runGlobalSequenceStep6() {

    startApplication('cctv-remote-commander');
    $('[data-application-id="cctv-remote-commander"]').css('left', (($(window).width()/2) - 200 )+ 'px'); 

    window.globalsequenceCheckForReadyStatusInterval = setInterval(function(){
        if ($('[data-application-id="cctv-remote-commander"]').attr('application-status') === 'ready') {
            
            runGlobalSequenceStep7();

            clearInterval(window.globalsequenceCheckForReadyStatusInterval);
        }
    }, window.checkingIntervalTime);
}

function runGlobalSequenceStep7() {

    task('connect-cctv').setCurrentTask();

    $('.cctv-remote-login-container .login-form-container #cctv-remote-login-form-ip-address').val([172,16,10,60].join('.'));
    $('.cctv-remote-login-container .login-form-container #cctv-remote-login-form-username').val('root');
    $('.cctv-remote-login-container .login-form-container #cctv-remote-login-form-password').val(window.clipBoard[28]);
    $('.cctv-remote-login-container .login-form-container #cctv-remote-login-form-submit').removeClass('disabled');

    setTimeout(function(){
        $('.cctv-remote-login-container .login-form-container #cctv-remote-login-form-submit').click();
        runGlobalSequenceStep8();
    }, 1000);
    
}

function runGlobalSequenceStep8() {
    task('connect-cctv').setDoneTask();
    task('hack-all-cctv').setCurrentTask();

    var camsContainersCount = $('.camara-control-container').length;
    var camsContainersCounter = 0;

    window.globalsequencehakCctvCamarasInterval = setInterval(function(){
        if(camsContainersCounter < camsContainersCount) {

            //.active find arrow back click
            $('.camara-control-container.active').find('.cam-grd-view-link').click();

            $('.camara-control-container').eq(camsContainersCounter).click();
            $('.camara-control-container').eq(camsContainersCounter).find('.controls-container').find('.control-item[data-remote-cam-hack-mode="disable"]').click();
            camsContainersCounter++;
        
        } else {
            clearInterval(window.globalsequencehakCctvCamarasInterval);
            task('hack-all-cctv').setDoneTask();
            
            // hack every active cam if interval fails

            runGlobalSequenceStep9();
        }

    }, 8000);
}

function runGlobalSequenceStep9() {
    focusOnGlobalAppSequencer();

    setTimeout(function(){
        $('.window-container:not(.focused-window)').closeWindow();
        task('clear-history').setDoneTask();
        task('clear-logs').setDoneTask();
        task('disconnect-cctv').setDoneTask();
        runGlobalSequenceStep12();
    }, 2000);
    
}

function runGlobalSequenceStep10() {

    task('search-radio-transmissor').setCurrentTask();
    $('[data-shortcut-for="radio-brocker"]').dblclick();

    $('[data-application-id="radio-brocker"]').css('left', (($(window).width()/2) - 200 )+ 'px'); 

    window.globalsequenceCheckForReadyStatusInterval = setInterval(function(){
        if ($('[data-application-id="radio-brocker"]').attr('application-status') === 'ready') {
            task('search-radio-transmissor').setDoneTask();
            task('hack-radio-transmissor').setCurrentTask();

            
            runGlobalSequenceStep11();

            clearInterval(window.globalsequenceCheckForReadyStatusInterval);
        }
    }, window.checkingIntervalTime); 
}

function runGlobalSequenceStep11() {
    
    initMarmotRadioBrockerLoadingLayout('global-sequencer-start-attack');

    window.checkForCompletedStatusInterval = setInterval(function(){
        if ($('[data-application-id="radio-brocker"]').attr('application-status') === 'completed') {
            task('hack-radio-transmissor').setDoneTask();

            clearInterval(window.checkForCompletedStatusInterval);

            runGlobalSequenceStep12();

        }
    }, window.checkingIntervalTime); 
}

function runGlobalSequenceStep12() {
    task('done').setDoneTask();
}