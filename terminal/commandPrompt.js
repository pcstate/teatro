$(document).on("keydown", function (e) {
    if (e.ctrlKey  &&  e.altKey  &&  (e.key === "t" || e.key === "T")) {
        $('[data-shortcut-for="command-prompt"]').dblclick();
    }
});

$('[data-shortcut-for="command-prompt"]').on('dblclick', function() {

    if (!$('[data-application-id="command-prompt"]').hasClass('hidden')) {
        return;
    }

    window.terminalCommandHistory = [];
    window.terminalCommandHistoryIndex = 0;

    window.terminalRSHOptions = {
        'remote_ip': false,
        'remote_connection': false,
        'remote_privs': false,
        'remote_uploaded_scripts': false,
        'remote_antivirus_unloaded': false,
        'local_script': 'dimitri-nx3r.py'
    };

    $('#command-prompt .prompt-wrapper .prompt-command-item:not(.default-command)').remove();
    $('#command-prompt .prompt-wrapper .prompt-command-item.default-command').show();
})

$('[data-application-id="command-prompt"]').on('click', function() {
    $('#prompt-command-input').focus();
})

$("#prompt-command-input").keydown(function(e) {

    notAllowedChars = [226, 111, 17]; //prevent html tags insertion

    if (notAllowedChars.includes(e.which)) {
        e.preventDefault();
        return;
    }

    if (e.key === "ArrowUp" || e.key === "ArrowDown") {

        if (window.terminalCommandHistory.length === 0) {
            return;
        }

        var placeholder = '';

        if (e.key === "ArrowUp") {
            if (window.terminalCommandHistoryIndex > 0) {
                window.terminalCommandHistoryIndex--;
            }
        }

        if (e.key === "ArrowDown") {
            if (window.terminalCommandHistoryIndex < (window.terminalCommandHistory.length)) {
                window.terminalCommandHistoryIndex++;
            }

            if (window.terminalCommandHistoryIndex == (window.terminalCommandHistory.length)) {
                clearPrompt();
            }
        }

        placeholder = window.terminalCommandHistory[window.terminalCommandHistoryIndex];
        $('#prompt-command-input').text(placeholder);
    }

    if (e.which === 13) {

        e.preventDefault();

        var command = $("#prompt-command-input").text().trim();

        if (command === '') {
            return;
        }

        handleCommand($("#prompt-command-input").text().trim());
        $('#prompt-command-input').html('');
    }

    // delete 15 first commands
    while($("#command-prompt .prompt-wrapper p.prompt-command-item").length > 15) {
         $("#command-prompt .prompt-wrapper p.prompt-command-item:nth-child(1)").remove();
     }
});

function launchConsoleFocus(){
    $('#prompt-command-input').focus();
}

function registerCommand(command) {

    if (command.trim() === '') {
        return;
    }

    if (window.terminalCommandHistory.includes(command)) {
        window.terminalCommandHistory = arrayRemove(window.terminalCommandHistory, command);
    }

    postPromptCommand(command);
    
    window.terminalCommandHistory.push(command);
    window.terminalCommandHistoryIndex = window.terminalCommandHistory.length;
}

function handleCommand(command) {

    registerCommand(command);

    var arguments = [];
    var firstCommand = '';

    if (command.indexOf(' ') !== -1) {

        //if there´s an option speratad by '' or "", prevent from spacing split
        var option = '';

        // replace all ' by "
        command = command.replace(/'/g, '"');

        if(command.indexOf('"') !== -1){

            // add ir as option and remove it from main command
            option = extractFromString(command, '"');
            command = command.substr(0, command.indexOf('"') - 1);
        }

        // split string into opptions array
        var commands = command.split(' ');

        // main command
        firstCommand = commands[0];

        // arguments for main command
        arguments = commands.slice(1, command.length);

    }else{
        firstCommand = command;
    }

    commandControllerClear();

    switch(firstCommand) {

        //simple commands:
        case "--help":
        case "help": commandControllerHelp(); break;
        case 'clear': commandControllerClear(); break;
        case 'lock': commandControllerLock(); break;
        case "exit": commandControllerExit(); break;
        case "about": commandControllerAbout(); break;
        case "global": commandControllerGlobalExec(); break;
        case "echo": commandControllerEcho(option); break;
        case "list": commandControllerList(arguments); break;
        case "run": commandControllerRun(arguments); break;
        case "kill": commandControllerKill(arguments); break;
        case 'net-tools': commandControllerNetTools(arguments); break;
        case 'rsh': commandControllerRsh(arguments, option); break;

        default:
            var text = firstCommand;

            if (!isNaN(text.charAt(0))) {
                firstCommand = !isNaN(eval(firstCommand)) ? eval(firstCommand) : firstCommand;
                appendCommand(newSuccessCommand(firstCommand));
                return;
            }

            commandNotFound(firstCommand);
    }
}

// command controllers functions
function commandControllerHelp() {
    var commands = createMultiLineResponse([
        newSuccessCommand('List of available commands: '),
        newInfoCommand('exit -> close the terminal'),
        newInfoCommand('clear -> clear current terminal prompt'),
        newInfoCommand('list <:options:> [--help] -> list available <:options:>'),
        newInfoCommand('lock -> lock current user session'),
        newInfoCommand('echo <:text:> -> show <:text:> on screen'),
        newInfoCommand('run <:application-name:> -> Starts specified application'),
        newInfoCommand('kill <:application-PID:> -> Stops specified application'),
        newInfoCommand('net-tools <:option:> [--help] -> Run Network Tools Application'),
        newInfoCommand('rsh -> Run remote device commander')
    ]);

    appendCommand(commands);
}

function commandControllerClear() {
    appendCommand(newSuccessCommand(''));
    $("#command-prompt .prompt-wrapper p.prompt-command-item").hide();
    $('#command-prompt .prompt-wrapper p.prompt-command-item:not(.default-command)').remove();
}

function commandControllerLock() {
    appendCommand(newSuccessCommand(''));
    $('#main-menu-lock-screen-button').click();
}

function commandControllerExit() {
    appendCommand(newSuccessCommand(''));
    $('[data-application-id="command-prompt"]').closeWindow();
}

function commandControllerAbout() {
    appendCommand(newSuccessCommand('Developed by yosyp andriyash, in 2020, along february and march'));
    appendCommand(newInfoCommand('Using JS + jQuery, pure CSS, HTML, PHP and mySql'));
    appendCommand(newInfoCommand('visit http://yosyp.andriyash.com to contact me'));
    appendCommand(newInfoCommand('Send me email to yosyp@andriyash.com'));
    appendCommand(newInfoCommand('follow me on instagram @yosyp_97'));
}

function commandControllerEcho(option) {
    if (option.length > 0) {
        appendCommand(newSuccessCommand(option));
    }else{
        appendCommand(newErrorCommand('use echo "some string"'));
    }
}

function commandControllerRun(arguments) {
    if (arguments.length === 1) {
        var arg = arguments[0];
        var applicationWindow = $('.window-container[data-application-name="' + arg + '"]');

        if (applicationWindow.length === 0) {
            appendCommand(newErrorCommand('Can´t find application or process ' + arg));
            return;
        }

        var inicializationId = applicationWindow.attr('data-application-id');
        var inicializer = $('.application-shortcut-item[data-shortcut-for="' + inicializationId + '"]:not(.hidden)');

        if (inicializer.length === 1) {
            appendCommand(newSuccessCommand('Starting application ' + arg));

            setTimeout(function() {
                inicializer.dblclick();
            }, 1200);
            return;

        }else{
            appendCommand(newErrorCommand('Can´t run application ' + arg + '. Probably one or more arguments is required to run this app'));
            return;
        }
    }

    appendCommand(newErrorCommand('incomplete command sequence, use run <:APP-NAME:> use list --apps to show available apps'));
}

function commandControllerKill(arguments) {
    if (arguments.length === 1) {
        var arg = arguments[0];
        var applicationWindow = $('.window-container[data-application-pid="' + arg + '"]:not(.hidden)');

        if (applicationWindow.length === 0) {
            appendCommand(newErrorCommand('Can´t stop application or process ' + arg + ', access denied'));
            return;

        }else{
            applicationWindow.closeWindow();
            appendCommand(newSuccessCommand('Stoped application ' + arg));
            return;
        }
    }

    appendCommand(newErrorCommand('incomplete command sequence, use kill <:APP-PID:>'));
}

function commandControllerList(arguments) {
    if (arguments.length === 1) {

        var arg = arguments[0];

        if (arg === '--help') {
            var commands = createMultiLineResponse([
                newSuccessCommand('Use list <:option:>, example: list --apps'),
                newInfoCommand('--apps -> List of available applications'),
                newInfoCommand('--apps-runing -> List runing applications')
            ]);
            appendCommand(commands);
            return;
        }

        if (arg === '--apps') {
            appendCommand(newWarningCommand('Installed apps on this system:'));
            $('[data-application-id]').each(function(index) {
                appendCommand(newSuccessCommand($(this).attr('data-application-name')));
            });
            return;
        }

        if (arg === '--apps-runing') {

            appendCommand(newWarningCommand('Runing applications currently'));
            appendCommand(newInfoCommand('Interface SubSystem Process | PID: 1412'));
            appendCommand(newInfoCommand('Driver Controller | PID: 1586'));
            appendCommand(newInfoCommand('Network Controller ETH1 | PID: 514'));
            appendCommand(newInfoCommand('Sotrage Management Subsystem | PID: 4201'));
            appendCommand(newInfoCommand('User Graphics Adapter Controller | PID: '));
            appendCommand(newInfoCommand('NetCat-Proxy VPN Agent | PID: 4512'));
            appendCommand(newInfoCommand('XHR Analyzer Subsystem | PID: 6214'));

            $('[data-application-id]:not(.hidden)').each(function(index) {
                appendCommand(newSuccessCommand($(this).attr('data-application-name') + ' | PID: ' + $(this).attr('data-application-pid')));
            });
            return;
        }

        commandNotFound('list ' + arg);
        return;
    }

    appendCommand(newErrorCommand('incomplete command sequence, use list --help to show posibilities'));
}

function commandControllerNetTools(arguments) {
    if (arguments.length === 1) {
        var arg = arguments[0];

        if (arg === '--help') {
            var commands = createMultiLineResponse([
                newSuccessCommand('Use net-tools <:option:>, example: net-tools --list-config'),
                newInfoCommand('--list-config -> Show local network configuration'),
                newInfoCommand('--network-scan -> Fast Scan of local network devices'),
            ]);
            appendCommand(commands);
            return;
        }

        if (arg === '--list-config') {
            var commands = createMultiLineResponse([
                newSuccessCommand('Ethernet #01 Intel ProSet 10/100/1000 GBit Adapter:'),
                newInfoCommand('FQDN/DHCP DNS NAME: HOME.LOCAL'),
                newInfoCommand('IPv6 Address: fe80::e094:166c:df79:f443%10'),
                newInfoCommand('IPv4 Address: 172.16.10.205'),
                newInfoCommand('SubNet Mask: 255.255.0.0'),
                newInfoCommand('IPv4 Gateway: 172.16.10.1'),
            ]);
            appendCommand(commands);
            return;
        }

        if (arg === '--network-scan') {
            appendCommand(newSuccessCommand('Launching Net Tools Network Scan (Fast Mode)'));
            setTimeout(function(){
                $('[data-shortcut-for="net-tools-network-scan"]').dblclick();
            }, 1200);
            return;
        }
    }
    appendCommand(newErrorCommand('incomplete command sequence, use net-tools --help to show network bundles and helpers'));
}

function commandControllerRsh(arguments, option) {

    if (arguments.length === 1) {
        var arg = arguments[0];

        if (arg === '--help') {
            var commands = createMultiLineResponse([
                newWarningCommand('--remote-prompt <:command:> -> Run command in remote host'),
            ]);
            appendCommand(commands);
            return;
        }

        //--remote-prompt <:command:>
        if(arg === '--remote-prompt'){
            if(option.trim().length > 0){
                appendCommand(newSuccessCommand('launching specified command in remote host'));
                postCctvCommand(option.trim());
                return;
            }
        }
    }

    appendCommand(newErrorCommand('incomplete command sequence, use rsh --help'));
}

function commandControllerGlobalExec() {
    appendCommand(newSuccessCommand('Launching script...'));
    setTimeout(function(){
        $('[data-shortcut-for="global-app-launcher"]').dblclick();
    }, 1200);
    
}

// console functions
function createMultiLineResponse(arrayComands) {
    return arrayComands.join('');
}

function commandNotFound(command) {
    appendCommand(newErrorCommand('error, command "' + command + '" <span class="error-hint">not found</span> | Use command --help'));
}

function appendCommand(command) {
    $("#command-prompt .prompt-wrapper").append(command);
    $('.prompt-command-item:not(.default-command)').fadeIn(200);
}

function newCommand(command, type) {
    if (isNaN(command) && command.indexOf('->') !== -1 ) {
        return "<p style='display:none;' class='prompt-command-item " + type + "'>" + command.replace('->', '<i class="fa fa-chevron-circle-right" style="color: rgb(8, 241, 117);"></i>') + "</p>";
    } 
    return "<p style='display:none;' class='prompt-command-item " + type + "'>" + command + "</p>";
}

function newErrorCommand(command) {
    playAudioError();
    return newCommand(command, 'error');
}

function newSuccessCommand(command) {
    playAudioClick();
    return newCommand(command, 'success');
}

function newWarningCommand(command) {
    return newCommand(command, 'warning');
}

function newInfoCommand(command) {
    return newCommand(command, 'success');
}

function clearPrompt() {
    $("#prompt-command-input").html('');
}