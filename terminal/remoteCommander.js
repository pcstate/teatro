//onload, reset all layout
$(document).ready(function(){
    $('#remote-commander-wrapper .step-option.login-container').on('click', function(){
        $('#remote-commander-wrapper .login-wrapper').fadeOut(400, function(){
    
            $('#remote-commander-wrapper .login-wrapper').addClass('hidden');
            $('#remote-commander-wrapper .loading-wrapper').hide();
            $('#remote-commander-wrapper .loading-wrapper').removeClass('hidden');
            $('#remote-commander-wrapper .loading-wrapper').fadeIn(400);
    
            setTimeout(function(){
                $('#remote-commander-wrapper .loading-wrapper').addClass('hidden');
                $('#remote-commander-wrapper .control-panel-wrapper').removeClass('hidden');
    
                //execute main app process
                runRemoteCommanderProcess();
            }, 5000);
    
        })
    })
})


function runRemoteCommanderProcess(){

    $('#remote-commander-wrapper .step-option').each(function(){
        $(this).attr('data-step-id', generateId());
    })

    //clean all blyat content
    $('#remote-commander-wrapper .step-option .step-progress').css('width', '0%');
    $('#remote-commander-wrapper .step-option').addClass('disabled');
    $('#remote-commander-wrapper .step-option').removeClass('active');
    $('#remote-commander-wrapper .step-option').removeClass('done');

    $('#remote-commander-wrapper .step-option.privileges-container').addClass('active');
    $('#remote-commander-wrapper .step-option.privileges-container').removeClass('disabled');

    var timeProcess = 23; //sec 
    var eachIntervaSteplDuration = 100; //ms
    var totalIntervalCounter = (timeProcess * 1000 )/eachIntervaSteplDuration;
    var intervalCounter = 0;

    var $activeStep = null;
    var $prevStep = null;
    window.commandIsPosted = false;

    window.runRemoteCommanderInterval = setInterval(function(){

        if(intervalCounter <= totalIntervalCounter){

            var totalProcessPercentage = calculateProgressPercent(totalIntervalCounter, intervalCounter);
            var stepPercent = 100;
            
            // process steps
            if(totalProcessPercentage >= 0 && totalProcessPercentage < 20){
                // upgrade privileges
                stepPercent = calculateProgressPercent(20, totalProcessPercentage);
                $activeStep = $('#remote-commander-wrapper .step-option.privileges-container');
                $prevStep = $activeStep;

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_1');

                }

            }else if(totalProcessPercentage >= 20 && totalProcessPercentage < 45){
                // antivirus disabling 
                stepPercent = calculateProgressPercent(25, totalProcessPercentage - 20);
                $activeStep = $('#remote-commander-wrapper .step-option.antivirus-container');

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_2');
                }

            }else if(totalProcessPercentage >= 45 && totalProcessPercentage < 70){
                // firewall disabling 
                stepPercent = calculateProgressPercent(25, totalProcessPercentage - 45);
                $activeStep = $('#remote-commander-wrapper .step-option.prompt-container');

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_3');
                }

            }else if(totalProcessPercentage >= 70 && totalProcessPercentage < 85){
                // script upload 
                stepPercent = calculateProgressPercent(15, totalProcessPercentage - 70);
                $activeStep = $('#remote-commander-wrapper .step-option.script-upload-container');

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_4');
                }

            }else if(totalProcessPercentage >= 85 && totalProcessPercentage < 100){
                // script run 
                stepPercent = calculateProgressPercent(15, totalProcessPercentage - 85);
                $activeStep = $('#remote-commander-wrapper .step-option.run-script-container');

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_5');
                }

            }else{
                // hack completed
                stepPercent = 100;
                $activeStep = $('#remote-commander-wrapper .step-option.done-container');
                $activeStep.addClass('done');
                $activeStep.siblings('.step-option').addClass('hidden');

                if(!window.commandIsPosted){
                    window.commandIsPosted = true;
                    postCctvCommand('remote_commander_status_step_6');
                }
            }

            // step add styling
            
            $activeStep.find('.step-progress').css('width', stepPercent + '%');

            // end prev step
            if($activeStep.attr('data-step-id') !== $prevStep.attr('data-step-id') && $prevStep.hasClass('active')){
                $prevStep.addClass('done');
                $prevStep.removeClass('active');
                $prevStep.find('.step-progress').css('width', '100%');

                $activeStep.removeClass('disabled');
                $activeStep.addClass('active');

                $prevStep = $activeStep;

                window.commandIsPosted = false;
            }

            intervalCounter++;

        }else{
            //app end
            clearInterval(window.runRemoteCommanderInterval);
        }

    }, eachIntervaSteplDuration);
}