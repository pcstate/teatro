$(document).ready(function(){
    // widgets interval

    $('.widget-item.ram-usage-widget .ram-content').text(Math.floor(window.widgetRamAmount / 1024));
    // Init ram memory usage (for document ready):
    increaseRamAmountUsage(widgetRamAmountUsage);

    //Init cpu usage ant cpu temp widgets
    setInterval(function(){
        $('.percent-ussage-text').text(calculateRandomNumber(window.widgetCpuUssageRange[0], window.widgetCpuUssageRange[1]));
        $('.widget-item.cpu-usage-widget .cpu-clock').each(function(index){
            var percent = calculateRandomNumber(window.widgetCpuUssageRange[0], window.widgetCpuUssageRange[1]);
            
            if(percent < 50){
                $(this).css('background-color', '#fff');
            }

            if(percent >= 50 && percent < 80){
                $(this).css('background-color', 'rgb(226, 238, 52)');
            }

            if(percent >= 80){
                $(this).css('background-color', 'rgb(161, 27, 27)');
            }

            $(this).css('height', percent + '%');
        })
    }, 1000)
})

// increment cpu usage, cpu temp and ram usage
function increaseCpuWidgetUssage(increment){
    increment = parseInt(increment);

    increaseRamAmountUsage(increment * 8);

    if( window.widgetCpuUssageRange[1] + increment >= 100){
        window.widgetCpuUssageRange = [90, 100];
    }else{
        window.widgetCpuUssageRange[0] = window.widgetCpuUssageRange[0] + increment;
        window.widgetCpuUssageRange[1] = window.widgetCpuUssageRange[1] + increment;
    }
}

function decreaseCpuWidgetUssage(decreasement){
    decreasement = parseInt(decreasement);

    decreaseRamAmountUsage(decreasement * 8);
    
    if( window.widgetCpuUssageRange[0] - decreasement <= 0){
        window.widgetCpuUssageRange = [0, 1];
    }else {
        window.widgetCpuUssageRange[0] = window.widgetCpuUssageRange[0] - decreasement;
        window.widgetCpuUssageRange[1] = window.widgetCpuUssageRange[1] - decreasement;
    }
}

// RAM usage widget funcitons
function increaseRamAmountUsage(increment){

    widgetRamAmountUsage += (increment);

    var percentRamUsage = Math.floor(calculateProgressPercent(window.widgetRamAmount, widgetRamAmountUsage));
    if(percentRamUsage > 90) {
        percentRamUsage = 90;
    }

    updateRamAmountUsage(percentRamUsage);
}

function decreaseRamAmountUsage(decreasement){

    widgetRamAmountUsage -= (decreasement);

    var percentRamUsage = Math.floor(calculateProgressPercent(window.widgetRamAmount, widgetRamAmountUsage));
    if(percentRamUsage < 10) {
        percentRamUsage = 10;
    }

    updateRamAmountUsage(percentRamUsage);
}

function updateRamAmountUsage(percentRamUsage) {
   $('.widget-item.ram-usage-widget .ram-amount-container').find('.used').text(percentRamUsage + '%');
   $('.widget-item.ram-usage-widget .ram-amount-container').find('.amount-percent').css('width', percentRamUsage + '%');
}