$(document).ready(function(){
    
    window.clientDesktopSessionId = 0;

    setInterval(function(){
        if(window.sessionStatus.receivedStatus.vault_room_is_hacked == 1) {
            $('.dali-bg-container').removeClass('hidden');
        } else {
            $('.dali-bg-container').addClass('hidden');
        }

        if (window.clientDesktopSessionId != window.sessionStatus.receivedStatus.session_id) {
            window.clientDesktopSessionId = window.sessionStatus.receivedStatus.session_id;

            //close all opened apps
            $('.window-container').each(function(){
                if(!$(this).hasClass('hidden') && !$(this).hasClass('minimized')) {
                    $(this).closeWindow();
                }
            })

            // look client account
            $('#main-menu-lock-screen-button').click();

            cleanGlobalSequenceIntervals();

        }

    }, 2000);
})