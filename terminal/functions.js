function createHexRandomString(length){
    var result           = '';
    var characters       = 'ABCDEF0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function createRandomString(length) {
    var result           = '0x';
    var characters       = 'ABCDEF0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length - 3; i++ ) {
        
        if(i%4===0 && i !== 0){
            result += " ";
        }else{
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        
    }
    return result;
 }

 function createRandomAsciiString(length) {
    var result           = '';
    var characters       = 'ABCDEF0123456789abcdefghijklmnñopqrstuvwxyz@!"·$%&/()=¿?^*,.;:Ç';
    var charactersLength = characters.length;
    for ( var i = 0; i <= length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

function hex_to_ascii(str1){
    var hex  = str1.toString();
    var str = '';
    for (var n = 0; n < hex.length; n += 2) {
        str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
    }
    return str;
}

function calculateProgressPercent(total, count) {
    return (100*count)/total;
}

function calculateRandomNumber(number1, number2) {
    var number1 = Math.ceil(number1);
    var number2 = Math.floor(number2);
    return Math.floor(Math.random() * (number2 - number1 + 1)) + number1;
}

$.fn.hasAttr = function(atribute){
    var attr = $(this).attr(atribute);
    if (typeof attr !== typeof undefined && attr !== false) {
        return true;
    }
    return false;
}

function makeRandomString(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function generateId(){
    return '_' + Math.random().toString(36).substr(2, 9);
}

function getAttributes ( node ) {
    var i,
        attributeNodes = node.attributes,
        length = attributeNodes.length,
        attrs = {};

    for ( i = 0; i < length; i++ ) attrs[attributeNodes[i].name] = attributeNodes[i].value;
    return attrs;
}

function validateIpAddress(ipaddress){
    if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)){
        return true;
    }
    return false
}

function arrayRemove(arr, value) {
    return arr.filter(function(ele) {
        return ele != value; 
    });
}

function degreesToRadians(deg){
    return (deg/180) * Math.PI;
}

function percentToRadians(percentage){
    // convert the percentage into degrees
    var degrees = percentage * 360 / 100;
    // and so that arc begins at top of circle (not 90 degrees) we add 270 degrees
    return degreesToRadians(degrees + 270);
}

function getTime(){
    var time = new Date();
    
    var currentTimeHours = time.getHours();
    var currentTimeMinutes = time.getMinutes();
    var currentTimeSeconds = time.getSeconds();

    if(currentTimeHours < 10) { currentTimeHours = '0' + currentTimeHours; }
    if(currentTimeMinutes < 10) { currentTimeMinutes = '0' + currentTimeMinutes; }
    if(currentTimeSeconds < 10) { currentTimeSeconds = '0' + currentTimeSeconds; }

    var time = currentTimeHours + ':' + currentTimeMinutes + ':' + currentTimeSeconds;

    return time;
}

function scrollToBottom(divContainer){
    $(divContainer).animate({
        scrollTop: $(divContainer).get(0).scrollHeight
    }, 500);
}

function extractFromString(string, char){
    var from = string.indexOf(char) + 1;
    var to = string.lastIndexOf(char);
    var count = to - from;

    var res = string.substr(from, count);
    return res;
}