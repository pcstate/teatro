//Metodo para poder arrastrar y soltar las notas

$.fn.calculatePosition = function(){

    var windowHeight = $(this).height();
    var windowWidth = $(this).width();

    var desktopHeight = $(window).height();
    var desktopWidth = $(window).width();

    // case just 1 displayed (this)
    if ($('.window-container:not(.hidden)').length === 0 || ($('.window-container:not(.hidden)').length === 1 && $('.window-container.minimized').length === 1)) {
        
        var positionTop = (desktopHeight / 2) - (windowHeight/2);
        var positionLeft = (desktopWidth / 2) - (windowWidth/2);

        $(this).css({'left' : positionLeft, 'top' : positionTop});
    
    } else {

        var focusedWindowPosition = getFocusedWindow().position();
        var focusedLeft = focusedWindowPosition.left;
        var focusedTop = focusedWindowPosition.top;

        $(this).css({
            'left': (focusedLeft + 75) + 'px',
            'top': (focusedTop + 75) + 'px',
        });
    }

    /*if ($('.window-container:not(.hidden)').length === 1 || ($('.window-container:not(.hidden)').length === 2 && $('.window-container.minimized').length === 1)) {

        var positionTop = (desktopHeight / 2) - (windowHeight/2);
        var positionLeft = (desktopWidth / 4) - (windowWidth/2);

        //This window go to right aside
        $(this).css({'top': positionTop, 'left': 'unset', 'right' : positionLeft});
        
        // First window go to left aside 
        $('.window-container:not(.hidden)').animate({'top': positionTop, 'left': positionLeft}, 400);
    
    } else if ($('.window-container:not(.hidden)').length > 1){

        var positionTop = (desktopHeight / 2) - (windowHeight/2);
        var positionLeft = (desktopWidth / 2) - (windowWidth/2);

        $(this).css({'left' : positionLeft, 'top' : positionTop});
    }

    // three windows displayed with this
    if ($('.window-container:not(.hidden)').length === 2) {

        var positionTop = ((desktopHeight/2) - windowHeight);
        var positionCenter = (desktopWidth / 2) - (windowWidth/2);
        var positionLeft = (desktopWidth / 4) - (windowWidth/2);
        
        //This window go to bottom center aside
        $(this).css({'top': 'unset', 'bottom' : positionTop, 'left': positionCenter});

        // First window go to top left aside 
        $('.window-container:not(.hidden)').eq(0).css({'top': positionTop, 'left': positionLeft});

        //This window go to top right aside
        $('.window-container:not(.hidden)').eq(1).css({'top': positionTop, 'left': 'unset', 'right' : positionLeft});

    }

    // four windows displayed with this
    if ($('.window-container:not(.hidden)').length === 3) {

        var positionTop = ((desktopHeight/2) - windowHeight);
        var positionLeft = (desktopWidth / 4) - (windowWidth/2);
        
        //This window go to bottom right aside
        $(this).css({'top': 'unset', 'bottom': positionTop, 'left': 'unset', 'right': positionLeft});

        // First window go to top left aside 
        $('.window-container:not(.hidden)').eq(0).css({'top': positionTop, 'left': positionLeft});

        //This window go to top right aside
        $('.window-container:not(.hidden)').eq(1).css({'top': positionTop, 'left': 'unset', 'right': positionLeft});

        //This window go to bottom left aside
        $('.window-container:not(.hidden)').eq(2).css({'top': 'unset', 'bottom': positionTop, 'left': positionLeft});
        
    }
    */

    

}

$.fn.moveWindow = function(){

    var headSelector = '.window-header-container';
    var headParentSelector = '.window-container';

    var nota_seleccionada = null;
    var arrastrando = false;

    var capturado_x, capturado_y;
    var start_x, start_y;
    var end_x, end_y;
    var id_nota;
    var window_width, window_height;
    var posicion_actual_x, posicion_actual_y;
    var atachedToOffset = false;

    //Cuando se presiona sobre el elemento al que se aplica la funcion
    $(this).find(headSelector).on("mousedown", function(e){

        e.preventDefault();

        nota_seleccionada = $(this).closest(headParentSelector);

        //Captura de la zona del titulo donde se presiono el mouse
        capturado_x = e.clientX - this.closest(headParentSelector).offsetLeft;
        capturado_y = e.clientY - this.closest(headParentSelector).offsetTop;

        //Captura del left y top (px) del elemento seleccionado respecto al window
        start_x =  this.closest(headParentSelector).offsetLeft;
        start_y = this.closest(headParentSelector).offsetTop;

        if(e.which === 1){
            arrastrando = true;
        }

    })

    //Mientras se mueva el raton/puntero y haya un posit agarrado (nota_seleccionada)
    $(this).closest(".client-desktop").on("mousemove", function(e){

        if (arrastrando && nota_seleccionada != null){

            e.preventDefault();

            if (e.clientX <= 20 || e.clientX >= $(window).width() - 20) {

                atachedToOffset = true;

                if (e.clientX <= 20) {
                    nota_seleccionada.addClass('show-back-offset');
                    nota_seleccionada.addClass('left');
                } else {
                    nota_seleccionada.addClass('show-back-offset');
                    nota_seleccionada.addClass('right');
                }

            } else {
                atachedToOffset = false;
                nota_seleccionada.removeClass('atached-offset-window');
                nota_seleccionada.removeClass('show-back-offset');
                nota_seleccionada.removeClass('left').removeClass('right');
            }

            posicion_actual_x = e.clientX - capturado_x;
            posicion_actual_y = e.clientY - capturado_y;

            if(posicion_actual_x < 10){
                posicion_actual_x = 0;
            }

            if(posicion_actual_x + nota_seleccionada.width() > $(window).width() - 10){
                posicion_actual_x = $(window).width() - nota_seleccionada.width();
            }

            if(posicion_actual_y < 10){
                posicion_actual_y = 0;
            }

            if(posicion_actual_y + nota_seleccionada.height() > $(window).height() - 40){
                posicion_actual_y = $(window).height() - nota_seleccionada.height() - 40;
            }


            nota_seleccionada.css({"left": posicion_actual_x + "px", "top": posicion_actual_y + "px", "opacity": 0.75});
        }
    })

    //Al soltar el elemento seleccionado (al dejar de presionar el click del raton)
    $(this).on("mouseup", function(e){

        if(nota_seleccionada != null){

            //Dejamos de presionar por tanto de arrastrar
            arrastrando = false;

            posicion_actual_x = roundTeenies(posicion_actual_x);
            posicion_actual_y = roundTeenies(posicion_actual_y);

            nota_seleccionada.css({
                opacity: 1
            });

            if (atachedToOffset) {
                nota_seleccionada.addClass('atached-offset-window');
            } else {
                nota_seleccionada.removeClass('atached-offset-window');
                nota_seleccionada.css({
                    "left": posicion_actual_x,
                    "top" : posicion_actual_y
                })
            }

            nota_seleccionada.removeClass('show-back-offset');

        }

        posicion_actual_x = null;
        posicion_actual_y = null;
        start_x = null;
        start_y = null;
        end_x = null;
        end_y = null;
        nota_seleccionada = null;
        id_nota = null;

    })
}

$.fn.superIndexOnFocus = function(){

    $(this).on("mousedown", function(){
        $(this).closest('.window-container').autoWindowSetZindex();
    });

};

$.fn.autoWindowSetZindex = function() {
    $(this).css('z-index', getMaxZindex('.window-container') + 1);
    $(this).updateBottomNavAppShortcut();

    $('.window-container').removeClass('focused-window');
    $(this).addClass('focused-window');
}

$.fn.updateBottomNavAppShortcut = function(){
    var appId = $(this).attr('data-application-id');

    // remove active class from all elements ?
    $('.bottom-navigator-container .active-apps-container .item.active').removeClass('active');

    var $bottomNavAppShortcut = $('.bottom-navigator-container .active-apps-container').find('.item[data-bottom-nav-link-to="' + appId + '"]');

    $bottomNavAppShortcut.addClass('active');
}

function focusNextZindexWindow() {

    var maxZindex = getMaxZindex('.window-container');

    $('.window-container').each(function(){
        var thisZindex = parseInt($(this).css('z-index'));

        if (!$(this).hasClass('hidden') && !$(this).hasClass('minimized') && (thisZindex === maxZindex)) {
            $(this).autoWindowSetZindex();
            return;
        }
    })
}

function getMaxZindex(selector){

    var max_value = 0;

    $(selector).each(function() {
        if (!$(this).hasClass('hidden') && !$(this).hasClass('minimized')) {
            var this_zIndex = parseInt($(this).css("z-index"));

            if(this_zIndex > max_value){
                max_value = this_zIndex;
            }
        }        
    });

    return max_value;
}

function getFocusedWindow(){ //max zindex 
    var selector = $('.window-container');
    var maxZindex = 0;
    var window = null;

    $(selector).each(function() {

        var this_zIndex = parseInt($(this).css("z-index"));

        if(this_zIndex > maxZindex && !$(this).hasClass('hidden')){
            maxZindex = this_zIndex;
            window = $(this);
        }
    });

    return window;
}

function roundTeenies(numero){

	numero = parseInt(numero, 10);

	var diferencia = numero % 10;

	if(diferencia == 0){
		return numero;
	}else if(diferencia > 5){
		numero = numero + (10 - diferencia);
	}else{
		numero = numero - diferencia;
	}

	return numero;
}

function clearCanvas(canvas) {
    var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function drawPercentageCircle(percentage, radius, canvas){
    var context = canvas.getContext('2d');
    canvas.style.backgroundColor = 'rgba(0,0,0,0)';

    var x = canvas.width / 2;
    var y = canvas.height / 2;
    var startAngle = percentToRadians(0);
    var endAngle = percentToRadians(percentage);
    // set to true so that we draw the missing percentage
    var counterClockwise = false;

    context.beginPath();
    context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
    context.lineWidth = 20;

    // line color
    context.strokeStyle = '#fff';
    context.stroke();
}