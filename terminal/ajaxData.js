function createNewSession(){
    $.ajax({
        url: window.remoteServerUrl + '?ajax_request=create_new_session',
        type: 'POST',
    });
}

window.sessionStatus = {
    receivedStatus: {
        session_name : '',
        session_id : '',
        user_config : {
            login_username : '',
            login_password : ''
        },
        target_cctv : {},
        target_ip_cams : {},
        target_radio_transmissor : {}
    }
};

resetToPostStatusInfo();
updateSessionInfo();


setInterval(function(){
    updateSessionInfo();
}, 500);

function updateSessionInfo()
{
    $.ajax({
        url: window.remoteServerUrl + '?ajax_request=update_session_status',
        type: 'POST',
        data: {
            post_session_status : JSON.stringify(window.sessionStatus.toSendStatus),
        }
    }).then(function(response) {

        resetToPostStatusInfo();

        var JsonSessionStatus = JSON.parse(response);
        window.sessionStatus.receivedStatus = JsonSessionStatus;

        updateLocalInfo();
        
    })
}

function resetToPostStatusInfo()
{
    window.sessionStatus.toSendStatus = {
        register_as_client : window.registerAsClient,
        post_prompt_commands : [],
        post_cctv_commands: [],
        target_ip_cams: {}
    }
}

function updateLocalInfo() {

    //session info
    window.serverSessionId = window.sessionStatus.receivedStatus.session_id;
    window.serverSessionName = window.sessionStatus.receivedStatus.session_name;

    //login config vars
    window.loginUsername = window.sessionStatus.receivedStatus.user_config.login_username;
    window.loginPassword = window.sessionStatus.receivedStatus.user_config.login_password;

    // cctv connection config
    window.remoteCctvAccessIp = window.sessionStatus.receivedStatus.target_cctv.target_cctv_ip;
    window.remoteCctvAccessUsername = window.sessionStatus.receivedStatus.target_cctv.target_cctv_username;
    window.remoteCctvAccessPassword = window.sessionStatus.receivedStatus.target_cctv.target_cctv_password;

    // radio brocker remote config
    window.radioBrockerIsHackedOnDb = window.sessionStatus.receivedStatus.is_hacked;
    window.radioTransmissionIsHacked = window.radioBrockerIsHackedOnDb;
    window.remoteRadioTransmissorIsActive = (!window.radioBrockerIsHackedOnDb);
    
}

function postPromptCommand(string){
    window.sessionStatus.toSendStatus.post_prompt_commands.push(string);
}

function postCctvCommand(string){
    window.sessionStatus.toSendStatus.post_cctv_commands.push(string);
}

function postRadioTransmissionIsOnHacking(){
    window.sessionStatus.toSendStatus.target_radio_transmissor.is_on_hacking = true;
}

function postRadioTransmissionIsHacked(){
    window.sessionStatus.toSendStatus.target_radio_transmissor.is_hacked = true;
}

function getRadioTransmissionIsHacked(){
    return false;
}

function postRemoteIpCamAction(action, value, ipCamChannel){
 
    window.sessionStatus.toSendStatus.target_ip_cams[ipCamChannel] = { [action]: value }
    
}

function postVaultRoomSessionStatusIsHacked() {
    window.sessionStatus.toSendStatus.vault_room_status = 'hacked';
}