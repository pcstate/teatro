$(document).ready(function(){
    $('[data-application-id="cctv-remote-commander"] .window-control.close').on('click', function(){
        stopCamStrem();
    })

    $('[data-shortcut-for="cctv-remote-commander"]').on('dblclick', function(){
        resetRemoteCctvLoginLayout();

        $('#cctv-remote-commander-wrapper .cctv-login-wrapper').removeClass('hidden');
        $('#cctv-remote-commander-wrapper .commander-wrapper').addClass('hidden');
    });

    setTimeout(function(){

        appendCctvCommanderCamaraItems();
        
    }, 1500);

    window.remoteDisplayedCams = JSON.stringify(window.sessionStatus.receivedStatus.target_cctv_display_cams);

    // if device cams to chow changes from remote, update layout
    setInterval(function(){
        if (window.remoteDisplayedCams != JSON.stringify(window.sessionStatus.receivedStatus.target_cctv_display_cams)) {
            
            appendCctvCommanderCamaraItems();
            restartCamStreaming();

            window.remoteDisplayedCams = JSON.stringify(window.sessionStatus.receivedStatus.target_cctv_display_cams);
        }
    }, 2000)
        
})


$('#cctv-remote-commander-wrapper .cctv-remote-login-container input[type="text"]').on('keyup', function(e){
    var ipAddress = $('#cctv-remote-login-form-ip-address').val().trim();

    if(!validateIpAddress(ipAddress)){
        $('#cctv-remote-login-form-ip-address').addClass('error');
        $('#cctv-remote-login-form-submit').addClass('disabled');
    }else {
        $('#cctv-remote-login-form-ip-address').removeClass('error');
        $('#cctv-remote-login-form-submit').removeClass('disabled');
    }
})

$('#cctv-remote-login-form-submit').on('click', function(e){
    e.preventDefault();

    if($(this).hasClass('disabled')){
        return;
    }

    $('[data-process-check="validate-police-system-ip"]').attr('data-process-check-target', window.remoteCctvAccessIp);
    $('[data-process-check="remote-cctv-login-username"]').attr('data-process-check-target', window.remoteCctvAccessUsername);
    $('[data-process-check="remote-cctv-login-password"]').attr('data-process-check-target', window.remoteCctvAccessPassword);

    var ipAddress = $('#cctv-remote-login-form-ip-address').val().trim();
    var username = $('#cctv-remote-login-form-username').val().trim();
    var password =  $('#cctv-remote-login-form-password').val().trim();

    $('[data-process-check="validate-police-system-ip"]').attr('data-process-check-value', ipAddress);
    $('[data-process-check="remote-cctv-login-username"]').attr('data-process-check-value', username);
    $('[data-process-check="remote-cctv-login-password"]').attr('data-process-check-value', password);

    $(this).hide();
    $(this).siblings('input').hide();
    $(this).siblings('.loading-frame').removeClass('hidden');

    runCctvConnectionScreen();
})

function appendCctvCommanderCamaraItems() {
    // remove from container:
    $('#cctv-remote-commander-wrapper .content.camara-control-section').find('.camara-control-container').remove();

    // create and append cctv video items
    var obj = window.sessionStatus.receivedStatus.target_cctv_display_cams;

    Object.keys(obj).forEach(key => {
        var channelName = obj[key];
        var video = window.sessionStatus.receivedStatus.target_ip_cams[channelName].video_src;
        var ip = window.sessionStatus.receivedStatus.target_ip_cams[channelName].ip_address;

        $('#cctv-remote-commander-wrapper .content.camara-control-section').append([
            '<div class="camara-control-container" data-cam-channel="' + channelName + '" data-video-ip="' + ip + '">',
                '<video id="' + channelName + '-video" muted loop>',
                    '<source src="../video/' + video + '" type="video/mp4">',
                '</video>',
                '<div class="camara-control-item no-signal">',
                    '<span class="cam-grd-view-link"><i class="fa fa-arrow-circle-left"></i></span>',
                    '<span class="cam-channel-text">#' + channelName + '</span>',
                    '<span class="cam-ip-text">' + ip + '</span>',
                    '<div class="lds-dual-ring"></div>',
                '</div>',
            '</div>'
        ].join(''));
    });

    var camaraControls = [
        '<div class="cam-controls">',
        '   <div class="controls-container">',
        '        <div class="control-item" data-remote-cam-hack-mode="disable">',
        '            <i class="fa fa-ban"></i>',
        '            <span class="title">Disable</span>',
        '        </div>',
        '        <div class="control-item" data-remote-cam-hack-mode="video-off">',
        '            <i class="fa fa-hand-peace-o"></i>',
        '            <span class="title">Hack</span>',
        '        </div>',
        '        <div class="control-item" data-remote-cam-hack-mode="download">',
        '            <i class="fa fa-download"></i>',
        '            <span class="title">Download</span>',
        '        </div>',
        '    </div>',
        '</div>'
    ].join('');

    $('.content#cctv-remote-commander-wrapper .camara-control-container .camara-control-item').each(function(){
        $(this).append(camaraControls);
    })

    $('.content#cctv-remote-commander-wrapper .camara-control-section .camara-control-container').on('click', function(e){
        if(!$(this).hasClass('active')){
            $(this).addClass('active');
            $(this).siblings('.camara-control-container').hide();
            e.stopPropagation();
        }
    })

    $('.content#cctv-remote-commander-wrapper .camara-control-section .camara-control-container .cam-grd-view-link').on('click', function(e){
        if($(this).closest('.camara-control-container').hasClass('active')){
            $(this).closest('.camara-control-container').removeClass('active');
            $(this).closest('.camara-control-container').siblings('.camara-control-container').show();
            e.stopPropagation();
        }
    })

    // cam controls
    $('.content#cctv-remote-commander-wrapper .camara-control-section .camara-control-item .cam-controls .control-item').on('click', function(){

        if($(this).hasClass('disabled')){
            return;
        }

        var camChannel = $(this).closest('.camara-control-container').attr('data-cam-channel');
        var action = $(this).attr('data-remote-cam-hack-mode');

        setTimeout(function(){
            postRemoteIpCamAction(action, 1, camChannel);
        }, 5250);

        switch(action){
            case 'disable': 

                var time = 5000;
                showRemoteIpHackProgress(time);
                
                setTimeout(function(){
                    postRemoteIpCamAction(action, 1, camChannel);
                }, time - 1000);

            break;

            //
            case 'video-off': 

                var time = 3000;
                showRemoteIpHackProgress(time);

                setTimeout(function(){
                    postRemoteIpCamAction(action, 1, camChannel);
                }, time - 500);

            break;
        }
    })
}

function showRemoteIpHackProgress(time){

    var intervalTime = (time)/100;

    // reset all progress values
    $('#cctv-remote-commander-wrapper .action-progress-container .progress-bar').css('width', '0%');
    $('#cctv-remote-commander-wrapper .action-progress-container .percentage').text('0');

    // show container
    $('#cctv-remote-commander-wrapper .action-progress-container').removeClass('hidden');
    window.showRemoteIpHackProgressIntervalProgress = 0;
    window.showRemoteIpHackProgressInterval = setInterval(function(){
        if(window.showRemoteIpHackProgressIntervalProgress <= 100){
            $('#cctv-remote-commander-wrapper .action-progress-container .progress-bar').css('width', window.showRemoteIpHackProgressIntervalProgress + '%');
            $('#cctv-remote-commander-wrapper .action-progress-container .percentage').text(window.showRemoteIpHackProgressIntervalProgress);
            window.showRemoteIpHackProgressIntervalProgress++;
        }else{
            clearInterval(window.showRemoteIpHackProgressInterval);
            $('#cctv-remote-commander-wrapper .action-progress-container').addClass('hidden');
        }
    }, intervalTime);
}

function runCctvConnectionScreen(){

    $('#cctv-remote-commander-wrapper .cctv-remote-login-container .connection-steps').attr('data-connection-step', 1);
    $('#cctv-remote-commander-wrapper .cctv-remote-login-container .connection-steps .step-item').addClass('hidden');
    $('#cctv-remote-commander-wrapper .cctv-remote-login-container .connection-error-messagge').addClass('hidden');

    window.cctvLayoutLastLoadingText = '';
    window.cctvConnectionDisplayInterval = setInterval(function(){

        var connectionSteps = $('.loading-frame .connection-steps .step-item').length;
        var currentConnectionStep = parseInt($('.cctv-remote-login-container .connection-steps').attr('data-connection-step'));

        if(currentConnectionStep <= connectionSteps){

            $('.cctv-remote-login-container .connection-steps .step-item').addClass('hidden');
            $('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').removeClass('hidden');

            if($('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').text().trim() === '') {
                $('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').text(window.cctvLayoutLastLoadingText);
            } else {
                window.cctvLayoutLastLoadingText = $('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').text();
            }

            if($('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').hasAttr('data-process-check')){

                var userValue = $('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').attr('data-process-check-value');
                var targetValue = $('.cctv-remote-login-container .connection-steps .step-item:nth-child(' + currentConnectionStep + ')').attr('data-process-check-target');
                //hacer aqui funcion para que si no coincidem no siga y muestre fallo.
                if (targetValue !== userValue) {
                    clearInterval(window.cctvConnectionDisplayInterval);
                    resetRemoteCctvLoginLayout(true);
                    return;
                }

            }

            $('.cctv-remote-login-container .connection-steps').attr('data-connection-step', currentConnectionStep + 1);
        }else{
            clearInterval(window.cctvConnectionDisplayInterval);
            $('.cctv-remote-login-container .connection-steps .step-item').addClass('hidden');

            $('#cctv-remote-commander-wrapper .cctv-login-wrapper').addClass('hidden');
            $('#cctv-remote-commander-wrapper .commander-wrapper').removeClass('hidden');
            //execute next function
            
            runCamStreaming();
        }

    }, 100);
}

function resetRemoteCctvLoginLayout(showError){
    $('#cctv-remote-commander-wrapper .cctv-remote-login-container input').show();
    $('#cctv-remote-commander-wrapper .cctv-remote-login-container .loading-frame').addClass('hidden');
    $('#cctv-remote-commander-wrapper .cctv-remote-login-container input').removeClass('error');

    if (showError === true) {
        $('#cctv-remote-commander-wrapper .cctv-remote-login-container .connection-error-messagge.hidden').removeClass('hidden');
    } else {
        $('#cctv-remote-commander-wrapper .cctv-remote-login-container .connection-error-messagge').addClass('hidden');
        $('#cctv-remote-commander-wrapper .cctv-remote-login-container input[type="text"]').val('');
    }
}

function stopCamStrem(){
    clearInterval(window.camVideoAssignTimeInterval);
    $('#cctv-remote-commander-wrapper .camara-control-container').removeAttr('data-video-time-is-sincronyzed');
}

function runCamStreaming(){
    
    $('.camara-control-container .camara-control-item:not(.no-signal)').addClass('no-signal');

    window.remoteCctvSessionId = window.sessionStatus.receivedStatus.session_id;
    window.availableCctvIpCams = [];

    $('#cctv-remote-commander-wrapper .camara-control-container').each(function(){
        window.availableCctvIpCams.push($(this).attr('data-cam-channel'));
    });

    window.camVideoAssignTimeInterval = setInterval(function() {
        
        if(window.remoteCctvSessionId !== window.sessionStatus.receivedStatus.session_id){
            clearInterval(window.camVideoAssignTimeInterval);
            restartCamStreaming();

            window.remoteCctvSessionId = window.sessionStatus.receivedStatus.session_id;
            return;
        }

        for(var i = 0; i < window.availableCctvIpCams.length; i++) {

            var currentCamEvaluation = window.availableCctvIpCams[i];

            var is_hacked = window.sessionStatus.receivedStatus.target_ip_cams[currentCamEvaluation].is_hacked;
            var is_paused = window.sessionStatus.receivedStatus.target_ip_cams[currentCamEvaluation].is_paused;
            var is_video_ko = window.sessionStatus.receivedStatus.target_ip_cams[currentCamEvaluation].is_video_ko;

            if(is_hacked == '1') {
                $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + currentCamEvaluation + '"] .camara-control-item').addClass('no-signal');
            } else {
                $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + currentCamEvaluation + '"] .camara-control-item').removeClass('no-signal');
            }

            if(is_video_ko == '1') {
                $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + currentCamEvaluation + '"]').addClass('video-ko');
            } else {
                $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + currentCamEvaluation + '"]').removeClass('video-ko');
            }

            var currentVideo = $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + currentCamEvaluation + '"]');

            if(!currentVideo.hasAttr('data-video-time-is-sincronyzed') && is_hacked !== "1"){

                currentVideo.attr('data-video-time-is-sincronyzed', 1);
                playChannelVideo(currentCamEvaluation, window.sessionStatus.receivedStatus.target_ip_cams[currentCamEvaluation].last_frame);

            }
        }
    }, 250);

}

function restartCamStreaming(){
    stopCamStrem();

    setTimeout(function(){
        runCamStreaming();
    }, 1000);
    
}

function playChannelVideo(camChannel, last_frame){

    var currentVideo = $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + camChannel + '"]');
    var currentVideoId = currentVideo.find('video').attr('id');

    var video = document.getElementById(currentVideoId);
    video.pause();
    video.currentTime = last_frame;
    video.play();
}

function pauseChannelVideo(camChannel){
    
    var currentVideo = $('#cctv-remote-commander-wrapper').find('.camara-control-container[data-cam-channel="' + camChannel + '"]');

    currentVideo.removeAttr('data-video-time-is-sincronyzed');
    var currentVideoId = currentVideo.find('video').attr('id');

    var video = document.getElementById(currentVideoId);
    video.pause();
}


/*
--
corregir posibles errores, 
probar con varios equipos, portatil etc
ver lo del error en pause del video cuando cambia manual en bd
*/