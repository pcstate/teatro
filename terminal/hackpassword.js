$(document).ready(function(){

    $('.ip-address-form-container input').on('keydown', function(e){
        var prevValue = $(this).val().trim();

        if(e.key === '.'){
            e.preventDefault();

            if(prevValue !== ''){
                $(this).next().focus();
            }
        }

        if(e.which === 8 && prevValue.length === 0) {
            $(this).prev().focus();
        }
    })

    $('.ip-address-form-container input').on('keyup change', function(e){

        var value = $(this).val().trim();

        if(value.length === 3 && typeof $(this).next() !== undefined) {
            $(this).next().focus();
        }

        var valueInt = parseInt(value);

        if(!isNaN(valueInt) && (valueInt >= 0 && valueInt < 255)){
            $(this).removeClass('error');
        }else{
            $(this).addClass('error');
        }

        if ($('input[class^="ip-addres-part"].error').length === 0) {
            $('#start-hack-password').removeClass('error');
        } else {
            $('#start-hack-password').addClass('error');
        }
    })

    $('#start-hack-password').on('click', function(){
        if($('input[class^="ip-addres-part"].error').length === 0) {
            var ipAddress = $('.ip-addres-part-1').val() + '.' + $('.ip-addres-part-2').val() + '.' + $('.ip-addres-part-3').val() + '.' + $('.ip-addres-part-4').val();
            $('.ip-address-reference').text(ipAddress);

            $('[data-process-check="validate-police-system-ip"]').attr('data-process-check-target', window.remoteCctvAccessIp);
            $('[data-process-check="validate-police-system-ip"]').attr('data-process-check-value', ipAddress);

            if (!$('[data-application-id="password-decryptor"]').hasClass('hidden')) {
                $('[data-application-id="password-decryptor"]').closeWindow();
            }

            $('[data-shortcut-for="password-decryptor"]').dblclick();
        }
    })

    var timeInterval = 5;

    window.randomMatches = calculateRandomNumber(2000, 3000);
    window.intervalCounter = 1;
    window.stringLength = 1;
    window.timeInterval = timeInterval;
    window.passwordDecryptorinterval = null;

    window.passwordUnhasherSound = new Audio('../audio/hacking_2.mp3');

    window.passwordUnhasherSound.addEventListener('ended', function(){
        window.passwordUnhasherSound.play();
    });


    window.passwordDecryptorSound = new Audio('../audio/hacking_1.mp3');

    window.passwordDecryptorSound.addEventListener('ended', function(){
        window.passwordDecryptorSound.play();
    });

    $('[data-application-id="password-decryptor"] .window-control.close').on('click', function(){

        clearInterval(window.passwordDecryptorinterval);

        window.randomMatches = calculateRandomNumber(2000, 3000);
        window.intervalCounter = 1;
        window.stringLength = 1;
        window.timeInterval = timeInterval;
        window.passwordDecryptorinterval = null;
        $('#password-decryptor-wrapper .progress-bar-container .progress').css('width', '0');
        $('#password-decryptor-wrapper .progress-bar-container .progress-text').text('');

        $('.tested-keys').find('li').remove();
        decreaseCpuWidgetUssage(20);
        window.passwordDecryptorSound.pause();
    })

    $('[data-application-id="password-unhasher"] .window-control.close').on('click', function(){
        clearInterval(window.unhasherInterval);
        decreaseCpuWidgetUssage(50);
        window.passwordUnhasherSound.pause();
    })
})

// password unhasher main function
function launchPasswordUnhasher(){

    $('.unhasher-table').html('');
    $('.last-letter').show();
    $('.unhasher-current').show();
    $('.unhasher-footer .block').removeClass('success');
    $('.unhasher-result').removeClass('selectionable');
    $('.unhasher-table').closest('.window-container').attr('application-status', '');
    increaseCpuWidgetUssage(50);

    window.passwordUnhasherSound.play();

    $('#global-app-launcher-wrapper .task-item[data-task-item-name="doing-unhasher"]').setCurrentTask();

    var height = 10;
    var width = 20;
    var maxToUnhash = height * width;

    window.unhasherMaxCounter = 100000;

    var tableContent = '';
    for( var i = 0; i < height; i++){
        tableContent += '<tr>';
        for( var j = 0; j < width; j++){
            tableContent += '<td>' + createHexRandomString(2) + '</td>';
        }
        tableContent += '</tr>';
    }

    $('.unhasher-table').html(tableContent);

    window.unhasherMaxTimeDuration = 20;
    window.unhasherStartedAt = Date.now();

    window.unhasherIntervalCounter = 0;
    window.unhasherInterval = setInterval(function(){

        // simulate calculating password
        if($('.unhasher-table tr td:not(.finded)').length > 0){

            let nowMoment = Date.now();
            let diff = Math.round((nowMoment - unhasherStartedAt)/1000);
  

            $('.unhasher-table tr td:not(.finded)').each(function(index){

                if($('.unhasher-table tr td:not(.finded)').length == 1 ){
                    $(this).addClass('finded');
                }else if(diff >= window.unhasherMaxTimeDuration) {
                    if(createHexRandomString(1) == 'F') {
                        $(this).addClass('finded');
                    }
                } else if(createHexRandomString(2) == 'FF'){
                    $(this).addClass('finded');
                }

                var hexStr = '';
                for(var i = 0; i < 40; i++){
                    hexStr += hex_to_ascii(createHexRandomString(2));
                }

                $('.unhasher-current').text(hexStr);

                $(this).text(createHexRandomString(3));

                //calculate unhashed password
                // window.remoteCctvAccessPassword
                var unhashedPercent = calculateProgressPercent(maxToUnhash, $('.unhasher-table tr td.finded').length);
                var finalLength = window.remoteCctvAccessPassword.length;

                var currentToCut = (unhashedPercent * finalLength)/ 100;
                var finalString = window.remoteCctvAccessPassword.substring(0, Math.floor(currentToCut));
                $('.last-letter').text(createRandomAsciiString(1));
                $('.unhasher-result').text(finalString);
            })
            
        } else {

            $('.last-letter').hide();
            $('.unhasher-current').hide();
            $('.unhasher-footer .block').addClass('success');
            $('.unhasher-result').addClass('selectionable');
            decreaseCpuWidgetUssage(50);

            clearInterval(window.unhasherInterval);

            window.passwordUnhasherSound.pause();
            window.netToolsbeepAudio.play();


            $('.unhasher-table').closest('.window-container').attr('application-status', 'completed');
        }

        window.unhasherIntervalCounter++;

        

    }, 10);

}

function launchPasswordDecryptorApplication(){

    increaseCpuWidgetUssage(20);

    window.passwordDecryptorSound.play();

    $('#global-app-launcher-wrapper .task-item[data-task-item-name="generate-private-key"]').setCurrentTask();

    window.passwordDecryptorinterval = setInterval(function(){

        if(window.intervalCounter <= window.randomMatches) {
            // simulate calculating password

            var randomString = createRandomString(window.stringLength);
            appendTestedKey(randomString, 25);


            if(window.stringLength < 76) {
                window.stringLength++;
            }

            var progressPercent =  Math.round(calculateProgressPercent(window.randomMatches, window.intervalCounter)) + '%';

            $('#password-decryptor-wrapper .progress-bar-container .progress').css('width', progressPercent);
            $('#password-decryptor-wrapper .progress-bar-container .progress-text').text(progressPercent);

            var powedCounter = Math.pow(window.intervalCounter, 2);
            $('#password-decryptor-wrapper .bottom-bar .counter').text(powedCounter);
            $('#password-decryptor-wrapper .bottom-bar .length').text(window.stringLength);

            window.intervalCounter++;

        } else {
            // simulate hash found
            clearInterval(window.passwordDecryptorinterval);
            $('#password-decryptor-wrapper .bottom-bar .match-counter').text('1');
            $('[data-shortcut-for="password-unhasher"]').dblclick();
            $('.unhash-string-reference').text($('.tested-keys li:last-of-type').text());

            decreaseCpuWidgetUssage(20);
            window.passwordDecryptorSound.pause();
            window.netToolsbeepAudio.play();
        }

        //window.scrollTo(0,document.querySelector("body").scrollHeight);

    }, window.timeInterval);
}

function appendTestedKey(string, limit) {

    if (string.trim().length > 0) {
        var item = '<li class="item">' + string + '</li>';
        $('.tested-keys').append(item);
    }

    if($('.tested-keys li').length > limit){
        $('.tested-keys').find('li:lt(1)').remove();
    }
}