$(document).ready(function(){

    // behavior thatallows loading window frame width time interval
    startWindowShowProcess();

    // add unique id for each window to can locate them in the future
    $('.window-container').each(function(index){
        $(this).attr('id', generateId());
        $(this).attr('application-status', '');
        $(this).attr('data-application-pid', calculateRandomNumber(1000, 9000));
    })

    // create desktop shortcuts name and icon
    $('.client-desktop .application-shortcuts-content .application-shortcut-item').each(function(){
        if ($(this).hasAttr('application-shortcut-icon')) {
            $(this).append('<div class="application-icon"></div>');
        }

        if ($(this).hasAttr('application-shortcut-name')) {
            $(this).append('<div class="application-name">' + $(this).attr('application-shortcut-name') + '</div>');
        }
    })

    //for each application desktop shortcut create item in main navigation menu
    $('.client-desktop .application-shortcuts-content .application-shortcut-item:not(.hidden)').each(function(){

        var appName = $(this).find('.application-name').text();
        var appIcon = $(this).find('.application-icon').css('background-image');
        var appTarget =  $(this).attr('data-shortcut-for');

        var menuApplication = [
            '<div class="shortcut-item" data-menu-shortcut-for="' + appTarget + '">',
                "<div class='app-icon' style='background-image:" + appIcon + "'></div>",
                '<div class="app-name">' + appName + '</div>',
            '</div>'
        ].join('');

        $('.main-menu-container .shortcuts-container').append(menuApplication);
        
    });

    $('[data-menu-shortcut-for]').on('click', function(){
        var targetShortcut = $(this).attr('data-menu-shortcut-for');
        $('[data-shortcut-for="' + targetShortcut + '"]').dblclick();
        $('.main-menu-container').addClass('hidden');
    })

    // then add functionality
    var windowHeaderControls = [
        '<div class="window-header-container">',
            '<div class="window-header-content">',
                '<span class="window-title"></span>',
                '<div class="window-header-controls-container">',
                    '<span class="window-control minimize"><i class="fa fa-minus" aria-hidden="true"></i></span>',
                    '<span class="window-control close"><i class="fa fa-times" aria-hidden="true"></i></span>',
                '</div>',
            '</div>',
        '</div>'
    ].join('');

    $('.window-container .window-content').prepend(windowHeaderControls);

    // add move window event and zindex
    $('.window-container').moveWindow();
    $('.window-container').superIndexOnFocus();

    //close windoe / exit application
    $('.window-container .window-control.close').on('click', function(){

        $(this).closest('.window-container').addClass('hidden');
        $(this).closest('.window-container').removeClass('minimized');
        $(this).closest('.window-container').css('z-index', '0');

        $(this).closest('[data-process-items-visible]').removeAttr('data-process-items-visible');

        var appId = $(this).closest('.window-container').attr('data-application-id');
        removeBottomNavShortcut(appId);

        decreaseCpuWidgetUssage($(this).closest('.window-container').attr('data-cpu-consumption'));

        // focus next window
        focusNextZindexWindow();
    })

    // minimization behavior
    $('.window-container .window-control.minimize').on('click', function(){
        var windowId = $(this).closest('.window-container').attr('data-application-id');
        $('[ data-bottom-nav-link-to="' + windowId + '"]').click();

        // focus next window
        $(this).closest('.window-container').css('z-index', '0');
        focusNextZindexWindow();
    })

    //When shortcut is clicked: window application launch proccess:::

    $('[data-shortcut-for]').on('dblclick', function() {
        playAudioClick();
        
        var targetAppText = $(this).attr('data-shortcut-for');
        var $targetApp = $('[data-application-id="' + targetAppText + '"]');

        // if is minimized window: restore it
        if ($targetApp.hasClass('minimized')) {
            var windowId = $targetApp.attr('data-application-id');
            $('[ data-bottom-nav-link-to="' + windowId + '"]').click();
        }

        // if is new window app:: launch it proccess::
        if ($targetApp.hasClass('hidden')) {
            $targetApp.hide();

            if (!$targetApp.hasAttr('data-process-items-visible')) {
                $targetApp.attr('data-process-items-visible', 0);
            }

            //add original height-width params: (for minimization effect)
            $targetApp.attr('data-window-original-height', $targetApp.css('height'));
            $targetApp.attr('data-window-original-width', $targetApp.css('width'));

            //create bottom bar reference (link) to oppened app
            var applicationIcon = $(this).find('.application-icon').css('background-image');
            var applicationId = $targetApp.attr('data-application-id');

            createBottomNavShorcut(applicationIcon, applicationId);
            increaseCpuWidgetUssage($targetApp.attr('data-cpu-consumption'));

            //Set window name (the same that shortcut have) if not empty
            if($(this).find('.application-name').text().trim() !== ''){
                $targetApp.find('.window-title').text($(this).find('.application-name').text());
            }else if($(this).hasAttr('data-explorer-item-name')){
                $targetApp.find('.window-title').text($(this).attr('data-explorer-item-name'));
            }else{
                $targetApp.find('.window-title').text('Executing C++ Script... | Compilation #' + createHexRandomString(16));
            }

            $targetApp.calculatePosition();
            $targetApp.autoWindowSetZindex();
            $targetApp.removeClass('hidden');
            $targetApp.find('.window-application-wrapper .process-item').hide();

            $targetApp.fadeIn(150);
        } else {
            $targetApp.autoWindowSetZindex();
        }
    })
})


function startWindowShowProcess(){
    setInterval(function(){
        $('[data-process-items-visible]').each(function(index){

            if ($(this).hasAttr('data-process-items-visible') && !$(this).hasClass('hidden')){

                var currentCounter = parseInt($(this).closest('[data-process-items-visible]').attr('data-process-items-visible'));
                //check if all process item will be displayed and remove [data-process-item-visible] this container and the loop ¿?
                //increase [data-process-items-visible] counter of this window item

                var childElements = $(this).find('.window-application-wrapper .process-item').length;

                if (currentCounter < childElements) {

                    // check data values if there are some validator
                    if($(this).find('.window-application-wrapper .process-item:nth-child(' + currentCounter + ')').hasAttr('data-process-check')){

                        var userValue = $(this).find('.window-application-wrapper .process-item:nth-child(' + currentCounter + ')').attr('data-process-check-value');
                        var targetValue = $(this).find('.window-application-wrapper .process-item:nth-child(' + currentCounter + ')').attr('data-process-check-target');
                        //hacer aqui funcion para que si no coincidem no siga y muestre fallo.
                        if (targetValue !== userValue) {
                            $(this).find('.window-application-wrapper .process-item:nth-child(-n+' + currentCounter + ')').show();
                            return;
                        }

                    } else {
                        $(this).find('.window-application-wrapper .process-item:nth-child(' + currentCounter + ')').show();
                    }

                    // Hide the first element if more than x elements will be displayed o k lo k
                    // if(currentCounter >= 16){
                    //    $(this).find('.window-application-wrapper .process-item:visible').eq(0).hide();
                    //    console.log('sadsadsad');
                    // }

                } else if(currentCounter == childElements){

                    $(this).find('.window-application-wrapper .process-item:nth-child(-n+' + currentCounter + ')').show();
                    $(this).find('.window-application-wrapper .process-item').hide();
                    $(this).find('.window-application-wrapper .process-item.main').fadeIn(function(){
                        $(this).closest('.window-container').attr('application-status', 'ready');
                    });


                    if($(this).find('.window-application-wrapper .process-item.main').hasAttr('data-silent-run')){
                        var silentRun = $(this).find('.window-application-wrapper .process-item.main').attr('data-silent-run');
                        $('#' + silentRun).click();
                    }

                } else {
                    $(this).closest('[data-process-items-visible]').removeAttr('data-process-items-visible');
                }

                $(this).closest('[data-process-items-visible]').attr('data-process-items-visible', currentCounter + 1);
            }
        })
    }, 50);
}

function removeBottomNavShortcut(appId){
    var selector = '[data-bottom-nav-link-to="' + appId + '"]';
    if($(selector).length > 0) {
        $(selector).fadeOut(300, function(){
            $(this).remove();
        })
    }
}

function createBottomNavShorcut(icon, appId){
    window.minimizationAnimationHasBeenCompleted = true;
    if(!icon){
        icon = 'url(../img/icons/script.png)';
    }

    var app = "<div style='display:none' class='item new-navigator-app-shortcut' data-bottom-nav-link-to='" + appId + "'><div class='icon' style='background-image:" + icon + ";'></div></div>";
    $('.active-apps-container').append(app);

    $('.new-navigator-app-shortcut').on('click', function(){

        var targetWindow = $('.window-container[data-application-id="' + appId + '"]');

        if(!$(this).hasClass('active') && !targetWindow.hasClass('minimized')){
            targetWindow.autoWindowSetZindex();
            return;
        }

        if(!window.minimizationAnimationHasBeenCompleted) {
            return;
        }else{
            window.minimizationAnimationHasBeenCompleted = false;
        }

        var posTop = $(targetWindow).css('top');
        var posLeft = $(targetWindow).css('left');

        var linkOffsetLeft = $(this).offset().left;

        if($(targetWindow).hasClass('minimized')){

            var left = $(this).attr('data-window-original-left');
            var top = $(this).attr('data-window-original-top');
            var height = $(targetWindow).attr('data-window-original-height');
            var width = $(targetWindow).attr('data-window-original-width');

            //se abre y se mueve a la posicion top left
            $(targetWindow).removeClass('minimized');
            $(targetWindow).autoWindowSetZindex();
            $(targetWindow).animate({'left': left, 'top': top, 'height': height, 'width': width, 'opacity': 1}, 200, function(){
                window.minimizationAnimationHasBeenCompleted = true;
            });

        }else{

            $(this).attr('data-window-original-left', posLeft);
            $(this).attr('data-window-original-top', posTop);

            // remove active class (currently is not focused):
            $(this).removeClass('active');

            $(targetWindow).animate({'left': linkOffsetLeft, 'top': '100%', 'height': 0, 'width': 0, 'opacity': 0}, 200, function(){
                $(this).addClass('minimized');
                window.minimizationAnimationHasBeenCompleted = true;
            })
        }
    })

    // advanced hover functions
    /*
    $('.new-navigator-app-shortcut').hover(function(){

        $('.window-container').each(function(){
            
            if($(this).attr('data-application-id') === appId){
                $(this).css({'opacity': '1', 'outline' : '5px solid #ec2b2b'}); 
                $(this).autoWindowSetZindex();
            }else{
                $(this).css({'opacity': '0.2', 'outline' : 'unset'}); 
            }
        })

    }, function(){
        $('.window-container').css({'opacity': '1', 'outline' : 'unset'});

    })
    */

    $('.new-navigator-app-shortcut').fadeIn(400, function(){
        $(this).removeClass('new-navigator-app-shortcut');
    });

    $.fn.closeWindow = function(){
        $(this).find('.window-header-controls-container .window-control.close').click();
    }
}

function startApplication(applicationId) {
    $('[data-shortcut-for="' + applicationId + '"]').dblclick();
}