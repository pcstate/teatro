// this data comes from rremote db --------------

window.clientDesktopId = 0;

// fake police CCTV-IP system (target)
window.remoteCctvAccessIp = '';
window.remoteCctvAccessUsername = '';
window.remoteCctvAccessPassword = '';

// remote radio transmissor is active
window.remoteRadioTransmissorIsActive = '';

// Login password
window.loginPassword = "";

// end data from remote db ---------------------



//clipboard
window.clipBoard = [];

//Real Audio transmission duration:
window.audioBrockerTimeDuration = 35;

// Desktop widget cpu ussage:
window.widgetCpuUssageRange = [0,1];

// Desktop widget and bottom bar cpu temperature
window.widgetCpuTemperature = [35, 37];

// Desktop widget ram memory amoun and usage
window.widgetRamAmount = 16384;
window.widgetRamAmountUsage = 986;

window.serverSessionId = 0;



// Remote real server path -> change it by real server-IP on production enviroment /¡\
window.remoteServerUrl = '../server/';

//Brightness toggle screen control like real SO:
window.bodyBrightness = 1;