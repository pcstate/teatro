$(document).ready(function(){

    window.camStreamingCanvas = null;
    window.currentStreamingVideo = null;
    window.videoElements = ['CH-01', 'CH-02', 'CH-03', 'CH-04'];
    window.videoElementsIndex = 0;

    window.camStreamingCanvas = document.createElement("canvas");
    window.camStreamingCanvas.style.display = "none";

    document.body.appendChild(window.camStreamingCanvas);

    var socket = new WebSocket("ws://localhost:4100");
    socket.binaryType = "arraybuffer";

    socket.onopen = function() {
       
    };

    setTimeout(function(){

        window.camStreamingCanvas.width = $('.video-item').eq(0).width();
        window.camStreamingCanvas.height = $('.video-item').eq(0).height();

        setInterval(function(){

            var currentCamChannel = window.videoElements[window.videoElementsIndex];
            var videoElementId = $('.display-item[id="' + currentCamChannel + '"]').find('.video-item').attr('id');
            var videoElement = document.getElementById(videoElementId);
            
            generateThumbnail(socket, videoElement, currentCamChannel);
    
            if(window.videoElementsIndex < window.videoElements.length - 1){
                window.videoElementsIndex++;
            }else{
                window.videoElementsIndex = 0;
            }
    
        }, 250);

    }, 250);
})

function generateThumbnail(socket, videoElement, camChannel) {
    //generate thumbnail URL data
    var context = window.camStreamingCanvas.getContext('2d');

    context.drawImage(videoElement, 0, 0, window.camStreamingCanvas.width, window.camStreamingCanvas.height);
    var dataURL = window.camStreamingCanvas.toDataURL();

    socket.send(dataURL);
    //console.log(dataURL);
    /*
    $.ajax({
        url: '../server/camstream.php?push',
        type: 'POST',
        data: {
            dataURL: dataURL,
            camChannel: camChannel
        },
        
        success: function(response){
            console.log(response);
        }
    });
    */
}