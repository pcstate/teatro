<?php

session_start();

header("Access-Control-Allow-Origin: *");

require_once('./conf.php');
require_once('./app/functions.php');
require_once('./app/dbConnector.php');

$dbConnection = new DBConnection();
define('CLIENT_CAMS_LIMIT', 4);

// get active session
$activeSession = $dbConnection->getActiveSession();

// if not session exists
if(!$activeSession){
    //create new gaming session
    $activeSession = $dbConnection->createActiveSession();
}

$activeSessionId = $activeSession['id'];



if(isset($_REQUEST['ajax_request'])){

    switch($_REQUEST['ajax_request']){

        case 'create_new_session': 
            createNewSession($dbConnection);
            // $activeSession = $dbConnection->getActiveSession();
        break;

        case 'update_session_status' :

            // first retreive all data from remote
            $receivedStatus = json_decode($_POST['post_session_status'], true);

            // client info
            $registeredClientData = null;

            $ipcamsStatus = $dbConnection->getAllIpCamsStatus($activeSessionId);
            $currentCctvCommands = $dbConnection->getLastestCctvCommands($activeSessionId);

            if(isset($receivedStatus['post_cctv_commands'])) {
                $commands = $receivedStatus['post_cctv_commands'];
                foreach($commands as $command) {
                    $dbConnection->registerCctvCommand(htmlspecialchars(trim($command)), $activeSessionId);
                }
            }

            if(isset($receivedStatus['post_prompt_commands'])) {
                $commands = $receivedStatus['post_prompt_commands'];
                foreach($commands as $command) {
                    $dbConnection->registerPromptCommand(htmlspecialchars(trim($command)), $activeSessionId);
                }
            }

            if (isset($receivedStatus['register_as_client'])) {
                
                $registeredAsClient = $receivedStatus['register_as_client'];
                // check if is client
                if ($registeredAsClient === true) {
                    // no registra al usuario 

                    if (!isset($_SESSION['remote_user_session_id'])) {
                        $_SESSION['remote_user_session_id'] = generateRandomString(24);
                    }

                    $clientIp = $_SESSION['remote_user_session_id'];

                    $registeredClientData = $dbConnection->getRegisteredClient($clientIp, $activeSessionId);

                    if ($registeredClientData === null || $registeredClientData === false) {
                        // create new usser in the database
                        $registeredClientData = $dbConnection->createRegisteredClient($clientIp, $activeSessionId);
                        
                        // assign cams to the created user
                        $databaseRegisteredCamsByClient = $dbConnection->getReservedCamarasByClients($activeSessionId);

                        if ($databaseRegisteredCamsByClient) {

                            $availableCamaras = [];
                            $reservedCams = [];
                            $toReserveCams = [];

                            foreach ($ipcamsStatus as $item) {
                                array_push($availableCamaras, $item['channel_name']);
                            }

                            foreach ($databaseRegisteredCamsByClient as $camsGroup) {

                                if ($camsGroup) {
                                    foreach ($camsGroup as $jsonCamGroup) {
                                        if (strlen(trim($jsonCamGroup)) > 0) {    
                                            foreach (json_decode($jsonCamGroup, true) as $camItem) {
                                                array_push($reservedCams, $camItem);  
                                            }
                                        }
                                    }
                                }
                                
                            }

                            $index = 0;

                            foreach ($availableCamaras as $value) {
                                if ($index < CLIENT_CAMS_LIMIT && !in_array($value, $reservedCams)) {
                                    array_push($toReserveCams, $value);
                                    $index++;
                                }
                            }

                            $reserveCamsToClient = json_encode($toReserveCams);
                            $registeredClientData = $dbConnection->addReservedCamsToClient($clientIp, $activeSessionId, $reserveCamsToClient);
                        }

                    }
                } else {
                    $_SESSION['remote_user_session_id'] = 'proyector_session';
                }
            }

            if(isset($receivedStatus['target_ip_cams']) && !empty($receivedStatus['target_ip_cams'])) {

                foreach($receivedStatus['target_ip_cams'] as $key => $channel) {

                    $action = key($channel);
                    $value = $channel[$action];

                    $camChannel = $key;
                    $action = htmlspecialchars(trim($action));
                    $value = htmlspecialchars(trim(strtoupper($value)));

                    $dbConnection->updateIpCamStatus($camChannel, $action, $value, $activeSessionId);
                }
            }
            
            if (isset($receivedStatus['vault_room_status'])  ) {
                if ($receivedStatus['vault_room_status'] === 'hacked') {
                    $dbConnection->setVaultRoomIsHacked($activeSessionId);

                    // hack all cams
                    $dbConnection->setAllIpCamsHacked($activeSessionId);
                }
            }

            // if it is client, get just assigned cams 
            $displayedCams = ['ch01', 'ch02', 'ch03', 'ch04', 'ch05', 'ch06', 'ch07','ch08'];

            if ($registeredClientData ) {
                $displayedCams = json_decode($registeredClientData['assigned_cams'], true);
            }

            foreach ($ipcamsStatus as $value) {
                $camsChannels[$value['channel_name']] = [
                    'last_frame' => $value['last_frame'],
                    'is_hacked' => $value['is_hacked'],
                    'is_paused' => $value['is_paused'],
                    'is_video_ko' => $value['is_video_ko'],
                    'video_src' => $value['video_src'],
                    'ubication_name' => $value['ubication_name'],
                    'ip_address' => $value['ip_address']
                ];
            }

            
            $vaultRoomStatus = $dbConnection->getVaultRoomStatus($activeSessionId);

            if (!$vaultRoomStatus) {
                $vaultRoomStatus = 0;
            } else {
                $vaultRoomStatus = $vaultRoomStatus['is_hacked'];
            }
            
            $response = [
                'server_session_uid' => isset($_SESSION['remote_user_session_id']) ? $_SESSION['remote_user_session_id'] : '__no__uid',
                'session_name' => $activeSession['session_name'],
                'session_id' => $activeSessionId,
                'user_config' => [
                    'login_username' => 'root',
                    'login_password' => '875432',
                ],
                'target_cctv' => [
                    'target_cctv_ip' => '172.16.10.60',
                    'target_cctv_username' => 'root',
                    'target_cctv_password' => 'p4ssw0rd202o_r00t',
                    'target_cctv_commands' => $currentCctvCommands
                ],
                'target_cctv_display_cams' => $displayedCams,
                'target_ip_cams' => $camsChannels,
                'target_radio_transmissor' => [
                    'is_on_hacking' => '',
                    'is_hacked' => (int) $dbConnection->getRadioTransmissor($activeSessionId)['is_hacked'] === 1
                ],
                'vault_room_is_hacked' => $vaultRoomStatus
            ];

            echo json_encode($response);
        break;

        case 'get_session_id':
            echo $activeSessionId;
        break;
    }

}