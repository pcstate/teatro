<?php

class DBConnection{
    
    private $conexion;
    private $query;
    
    public function __construct(){

        $ddbb_server = DDBB_SERVER;
        $ddbb_name = DDBB_NAME;
        $ddbb_user = DDBB_USER;
        $ddbb_password = DDBB_PASSWORD;

        try{
    
            $this->conexion = new PDO("mysql:host=$ddbb_server;dbname=$ddbb_name", $ddbb_user, $ddbb_password);
            $this->conexion->exec("set names utf8");

        }catch(PDOException $e){
            echo $e->getMessage(); die;
        }
    }

    public function getActiveSession($target = null){

        $this->query = $this->conexion->prepare("select * from sessions where is_active = 1 order by id desc limit 1");
        $this->query->execute();

        $contenido = $this->query->fetch(PDO::FETCH_ASSOC);

        if(isset($target)){
            return $contenido[$target];
        }

        return $contenido;

    }

    public function createActiveSession(){
        // close active sessions
        $this->closeSession();

        // create new session
        $this->createSession();

        // return active session
        return $this->getActiveSession();
    }

    public function createSession(){
        
        $sessionKey = generateRandomString(32);
        $isActive = 1;

        $this->query = $this->conexion->prepare('insert into sessions (session_name, is_active) values (:session_name, :is_active)');
        $this->query->execute(array(':session_name' => $sessionKey, ':is_active' => $isActive));
    }

    public function closeSession($sessionId = null) {
        if(isset($sessionId)){

            $this->query = $this->conexion->prepare('update sessions set is_active = 0');
            $this->query->execute(array(':id' => $sessionId));

        }else{

            $this->query = $this->conexion->prepare('update sessions set is_active = 0 where id = :id');
            $this->query->execute();

        }
    }

    public function registerPromptCommand($string, $sessionId){
        $this->query = $this->conexion->prepare('insert into console_commands (content, round_session_id) values (:string, :session_id)');
        $this->query->execute(array(':string' => $string, ':session_id' => $sessionId));
    }

    public function getAllIpCamsStatus ($sessionId) {
        $this->query = $this->conexion->prepare('select ip_cams_state.*, ip_cams.* from ip_cams_state join ip_cams on ip_cams_state.id_ip_cam = ip_cams.id where ip_cams_state.round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));

        $data = $this->query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function getIpCams($id = null){
        $query = 'select * from ip_cams where 1 = 1';
        if(isset($id)){
            $query .= ' and id = ' . $id;
        }

        $this->query = $this->conexion->prepare($query);
        $this->query->execute();

        $data = $this->query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function getIpCamsState($camChannel, $sessionId){
        $this->query = $this->conexion->prepare('select * from ip_cams_state where round_session_id = :session_id and id_ip_cam = (select id from ip_cams where channel_name = :channel_name)');
        $this->query->execute(array(':channel_name' => $camChannel, ':session_id' => $sessionId));

        $data = $this->query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function createActiveIpCams($sessionId){
        $ipCams = $this->getIpCams();

        foreach($ipCams as $ipCam){
            $id = $ipCam['id'];
            $this->query = $this->conexion->prepare('insert into ip_cams_state (id_ip_cam, last_frame, is_hacked, is_paused, is_video_ko, round_session_id) values (:id_ip_cam, "", "0", "0", "0", :session_id)');
            $this->query->execute(array(':id_ip_cam' => $id, ':session_id' => $sessionId));
        }

        return $ipCams;
    }

    public function setAllIpCamsHacked ($sessionId) {
        $this->query = $this->conexion->prepare('update ip_cams_state set is_hacked = 1 where round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));
    }

    public function updateIpCamStatus($camChannel, $action, $value, $sessionId){

        $field = '';

        switch ($action){
            case 'disable': 
                $field = 'is_hacked';
            break;

            case 'video-off': 
                $field = 'is_video_ko';
            break;

            case 'pause':
                $field = 'is_paused';
            break;

            default : $field = $action;
        }
        
        $this->query = $this->conexion->prepare('update ip_cams_state set ' . $field . ' = :value where round_session_id = :session_id and id_ip_cam = (select id from ip_cams where channel_name = :channel_name)');
        $this->query->execute(array(':session_id' => $sessionId, ':value' => $value, ':channel_name' => $camChannel));
    }

    public function getRadioTransmissor($sessionId){
        $this->query = $this->conexion->prepare('select * from radio_transmissions where round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));

        $data = $this->query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function setRadioTransmissorHacked($sessionId = null){
        $query = 'update radio_transmissions set is_hacked = 1 where 1 = 1';

        if(isset($sessionId)){
            $query .= ' and round_session_id = ' . $sessionId;
        }

        $this->query = $this->conexion->prepare($query);
        $this->query->execute();
    }

    public function setRadioTransmissorOnHacking($sessionId){
        $this->query = $this->conexion->prepare('update radio_transmissions set is_on_hacking = 1 where round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));
    }

    public function closeRadioTransmissionSession($sessionId = null){
        $this->setRadioTransmissorHacked($sessionId);
        $this->setRadioTransmissorOnHacking($sessionId);
    }

    public function createActiveRadioTransmissor($sessionId){
        $this->query = $this->conexion->prepare('insert into radio_transmissions (is_hacked, is_on_hacking, round_session_id) values ("0", "0", :session_id)');
        $this->query->execute(array(':session_id' => $sessionId));
    }

    public function insertCctvCommand($command, $ip, $sessionId){
        $this->query = $this->conexion->prepare('insert into cctv_commands (command, displayed, client_ip_address, round_session_id) values (:command, "0", :ip, :session_id)');
        $this->query->execute(array(':command' => $command, ':ip' => $ip, ':session_id' => $sessionId));
    }

    public function setCctvCommandIsDisplayed($id){
        $this->query = $this->conexion->prepare('update cctv_commands set displayed = 1 where id = :id');
        $this->query->execute(array(':id' => $id));
    }

    public function registerCctvCommand($string, $sessionId){
        $ip = '172.16.10.90';
        $this->insertCctvCommand($string, $ip, $sessionId);
    }
    
    public function getLastestCctvCommands($sessionId){
        $this->query = $this->conexion->prepare('select * from cctv_commands where round_session_id = :session_id and displayed = 0');
        $this->query->execute(array(':session_id' => $sessionId));

        $commands = $this->query->fetchAll(PDO::FETCH_ASSOC);
        $content = array();

        foreach($commands as $command){
            $id = $command['id'];
            array_push($content, $command['command']);
            $this->setCctvCommandIsDisplayed($id);
        }

        return $content;
    }

    public function getRegisteredClient($ip, $sessionId) {
        $this->query = $this->conexion->prepare('select * from session_clients where round_session_id = :session_id and remote_ip = :remote_ip order by id desc limit 1');
        $this->query->execute(array(':remote_ip' => $ip, ':session_id' => $sessionId));

        $data = $this->query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function createRegisteredClient ($ip, $sessionId) {
        $this->query = $this->conexion->prepare('insert into session_clients (round_session_id, remote_ip) values (:session_id, :remote_ip)');
        $this->query->execute(array(':remote_ip' => $ip, ':session_id' => $sessionId));

        return $this->getRegisteredClient($ip, $sessionId);
    }

    public function getReservedCamarasByClients($sessionId) {
        $this->query = $this->conexion->prepare('select assigned_cams from session_clients where round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));

        $data = $this->query->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    public function addReservedCamsToClient($ip, $sessionId, $reserveCamsToClient) {
        $this->query = $this->conexion->prepare('update session_clients set assigned_cams = :assigned_cams where remote_ip = :remote_ip and round_session_id = :round_session_id');
        $this->query->execute(array(':assigned_cams' => $reserveCamsToClient, ':remote_ip' => $ip, ':round_session_id' => $sessionId));

        return $this->getRegisteredClient($ip, $sessionId);
    }

    public function createVaultRoomStatus($sessionId) {
        $this->query = $this->conexion->prepare('insert into vault_room_status (round_session_id) values (:session_id)');
        $this->query->execute(array(':session_id' => $sessionId));
    }

    public function getVaultRoomStatus($sessionId) {
        $this->query = $this->conexion->prepare('select * from vault_room_status where round_session_id = :session_id');
        $this->query->execute(array(':session_id' => $sessionId));

        $data = $this->query->fetch(PDO::FETCH_ASSOC);
        return $data;
    }

    public function setVaultRoomIsHacked($sessionId) {
        $this->query = $this->conexion->prepare('update vault_room_status set is_hacked = 1 where round_session_id = :round_session_id');
        $this->query->execute(array(':round_session_id' => $sessionId));
    }

}