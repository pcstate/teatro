<?php

function generateRandomString($length){
    $dictionary = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';
    
    for($i = 0; $i < $length; $i++){
        $string .= substr($dictionary, rand(0, strlen($dictionary) - 1), 1);
    }

    return $string;
}

function createNewSession($ddbbConection){

    // get active session
    $activeSessionId = $ddbbConection->getActiveSession('id');

    // close old session
    $ddbbConection->closeSession($activeSessionId);

    // create new session
    $newActiveSession = $ddbbConection->createActiveSession();

    // initialize ip cams for this new session
    $ddbbConection->createActiveIpCams($newActiveSession['id']);

    // create radio transmission session
    $ddbbConection->createActiveRadioTransmissor($newActiveSession['id']);

    // create vault security server session
    $ddbbConection->createVaultRoomStatus($newActiveSession['id']);

}
