create database if not exists teatro character set utf8 collate utf8_general_ci;
use teatro;

-- create user 'teatro_user'@'localhost' identified by 'udfoiaydsfsdpfywoui3y4r78';
-- grant all privileges on teatro.* to 'teatro_user'@'localhost';
-- flush privileges;

create table sessions (
    id int(32) auto_increment not null,
    session_name varchar(128) not null unique,
    is_active boolean not null default '0',
    start_datetime datetime not null default CURRENT_TIMESTAMP,
    end_datetime datetime not null default CURRENT_TIMESTAMP,
    primary key(id)
) engine = innodb default charset=utf8 auto_increment = 1;

create table ip_cams (
    id int(32) auto_increment not null,
    channel_name varchar(16) not null,
    ubication_name varchar(64) not null default '',
    ip_address varchar(32) not null default '',
    primary key(id)
) engine = innodb default charset=utf8 auto_increment = 1;

create table ip_cams_state (
    id int(32) auto_increment not null,
    id_ip_cam int(32) not null,
    last_frame varchar(64) not null,
    is_hacked boolean not null,
    is_paused boolean not null,
    is_video_ko boolean not null,
    round_session_id int(32) not null,
    primary key(id),
    foreign key(id_ip_cam) references ip_cams(id) on delete cascade on update cascade,
    foreign key(round_session_id) references sessions(id) on delete cascade on update cascade
) engine = innodb default charset=utf8 auto_increment = 1;

create table cctv_commands (
    id int(32) auto_increment not null,
    date_time datetime not null default CURRENT_TIMESTAMP,
    command varchar(64) not null default '',
    displayed boolean not null default 0,
    client_ip_address varchar(32) not null default '',
    round_session_id int(32) not null,
    primary key(id),
    foreign key(round_session_id) references sessions(id) on delete cascade on update cascade
) engine = innodb default charset=utf8 auto_increment = 1;

create table radio_transmissions (
    id int(32) auto_increment not null,
    is_hacked boolean not null default 0,
    is_on_hacking boolean not null default 0,
    round_session_id int(32) not null,
    primary key(id),
    foreign key(round_session_id) references sessions(id) on delete cascade on update cascade
) engine = innodb default charset=utf8 auto_increment = 1;

create table console_commands (
    id int(32) auto_increment not null,
    content varchar(64) not null default '',
    round_session_id int(32) not null,
    primary key(id),
    foreign key(round_session_id) references sessions(id) on delete cascade on update cascade
) engine = innodb default charset=utf8 auto_increment = 1;

-- data insertions
/*
create table ip_cams (
    id int(32) auto_increment not null,
    channel_name varchar(16) not null,
    ubication_name varchar(64) not null default '',
    ip_address varchar(32) not null default '',
    primary key(id)
) engine = innodb default charset=utf8 auto_increment = 1;

*/


insert into ip_cams (channel_name, ubication_name, ip_address) values ('CH-01', 'Entrada Principal', '172.16.10.201');
insert into ip_cams (channel_name, ubication_name, ip_address) values ('CH-02', 'Salida Principal', '172.16.10.202');
insert into ip_cams (channel_name, ubication_name, ip_address) values ('CH-03', 'Patio 1', '172.16.10.203');
insert into ip_cams (channel_name, ubication_name, ip_address) values ('CH-04', 'Patio 2', '172.16.10.204');