<?php 

require_once('app/dbConnector.php');

$dbConnection = new DBConnection();

// get active session
$activeSession = $dbConnection->getActiveSession();

if(!$activeSession){
    die('no session has been initialized');
}

$activeSessionId = $activeSession['id'];

header("Access-Control-Allow-Origin: *");

if(isset($_GET['push'])){
    $name = time() . explode(' ', str_replace('0.', '', microtime()))[0];;
    $camChannel = $_POST['camChannel'];
    $camStreamFrame = isset($_POST['dataURL']) && !empty(trim($_POST['dataURL'])) ? proccessAjaxImage($_POST['dataURL'], $name, $camChannel) : 'no-image';

    $dbConnection->updateIpCamStatus($camChannel, 'last_frame', $camStreamFrame, $activeSessionId);

}else{
    echo '0';
}

function proccessAjaxImage($imageData, $name, $camChannel){

    $image_array_1 = explode(";", $imageData);
    $image_array_2 = explode(",", $image_array_1[1]);
    $imageData = base64_decode($image_array_2[1]);

    $iconBaseName = $name . '.png';

    $fullWebIconPath = 'cam-stream/' . $camChannel . '/' . $iconBaseName;

    file_put_contents($fullWebIconPath, $imageData);

    removeOldPictures($camChannel);

    return $fullWebIconPath;
}

function removeOldPictures($camChannel){
    $dir = 'cam-stream/' . $camChannel . '/';
    $dirContent = array_diff(scandir($dir), array('..', '.'));

    if(count($dirContent) > 5){
        $toRemove = array_slice($dirContent, 0, count($dirContent) - 6);
        foreach($toRemove as $value){
            $file = 'cam-stream/' . $camChannel . '/' . $value;
            unlink($file);
        }
    }
}
